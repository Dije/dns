<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
  <!-- Page Preloder -->
  <div id="preloder">
    <div class="loader"></div>
  </div>

  <!-- Navbar -->
  <?php include 'include/navbar.php' ?>
  <!-- Navbar -->

  <!-- Cta Section Begin -->
  <section class="cta-section spad set-bg" data-setbg="img/growth-journey-page-title.jpg">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cta-text">
            <h2>Growth Journey</h2>
            <p>About Us</p>
            <!-- <a href="#" class="primary-btn">Contact us</a> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Cta Section End -->

  <ul class="timeline"> 
    <li>
      <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-7-_resized360x260.jpg" alt="" /> </div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">2013</h4>
          <p>Our coal trading subsidiary, PT DNS</p>
        </div>
        <div class="timeline-body">
          <p>Focus on the domestic sale of coal
          Established our coal trading subsidiary, PT DNS, to consolidate all coal trading activities in the Group and to focus on the domestic sale of coal.</p> 
        </div>
      </div>
    </li>
    <li class="timeline-inverted">
      <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-12-_resized360x260.jpg" alt="" /> </div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">2014</h4>
          <p>PT DNS was granted the SIUP-M licence</p>
        </div>
        <div class="timeline-body">
          <p>Granted the SIUP-M licence
          PT DNS was granted the SIUP-M licence as distributor, exporter and importer of coal, iron ore, iron sand/manganese.</p>
        </div>
      </div>
    </li>
    <li>
      <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-10-_resized360x260.jpg" alt="" /> </div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">2017</h4>
          <p>PT DNS received a three-year IUP-OPK licence to source coal from four coal mines in South Kalimantan.</p>
        </div>
        <div class="timeline-body">
          <p>Three-year IUP-OPK licence
          PT DNS received a three-year IUP-OPK licence to source coal from four coal mines in South Kalimantan. The license is renewable for a maximum of three years for each renewal. We commenced our coal trading business.</p>
        </div>
      </div>
    </li>
    <li class="timeline-inverted">
      <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-11-_resized360x260.jpg" alt="" /> </div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">2019</h4>
          <p>Major achievements</p>
        </div>
        <div class="timeline-body">
          <p>Major Achievements
            Milestone 1
            PT DNS secured fixed term coal sale contracts from customers for aggregate sales of up to 1.5 million metric tonnes (“MT”) of coal, and fixed term coal purchase contracts from suppliers for the aggregate supply of up to 1.9 million MT of coal.

            Milestone 2
          PT DNS was granted a new five-year IUP-OPK licence for the transportation and sale of coal from four coal mines in South Kalimantan. The licence is renewable for a maximum of five years for each renewal. </p> 
        </div>
      </div>
    </li> 
  </ul>

  <!-- Footer -->
  <?php include 'include/footer.php' ?>
  <!-- Footer -->
</body>

</html>