<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<?php  
$idu = $_GET['id'];

if ($user->get_session() == true) {  
    $dataEdit = $financialHighlight->getDataByUid($idu);  

    if (isset($_POST['editFinH'])){ 
        $title = $_POST['judul'];
        $year = $_POST['tahun'];
        $desc = $_POST['desc'];

        if(isset($_FILES["file2"]) && !empty($_FILES["file2"]["name"])){
        $ekstensi_diperbolehkan2 = array('pdf');
        $nama2 = $_FILES['file2']['name'];
        $y = explode('.', $nama2);
        $ekstensi2 = strtolower(end($y));
        $ukuran2 = $_FILES['file2']['size'];
        $file_tmp2 = $_FILES['file2']['tmp_name'];
            if(in_array($ekstensi2, $ekstensi_diperbolehkan2) === true){
                if($ukuran2 < 150*MB){    
                    $update = $financialHighlight->updateDataByUID($title, $year, $desc, $nama2, $date, $idu);
                    if($update){
                        move_uploaded_file($file_tmp2, 'assets/pdf/FinancialHighlight/'.$nama2);
                        chmod('assets/pdf/FinancialHighlight/'.$nama2, 0777);
                        echo "<script type='text/javascript'>alert('Financial Highlight Update Success');</script>";
                    }else{
                        echo "<script type='text/javascript'>alert('Financial Highlight Update Failed. PDF exsist');</script>";
                    }
                }else{
                    echo "<script type='text/javascript'>alert('File Too Big');</script>";
                }
            }else{
                echo "<script type='text/javascript'>alert('Extension Is Not Allowed');</script>";
            }
        }else{
            $update = $financialHighlight->updateDataWithoutFileByUID($title, $year, $desc, $date, $idu);
            if($update){
                echo "<script type='text/javascript'>alert('Financial Highlight Update Success');</script>";
            }else{
                echo "<script type='text/javascript'>alert('Financial Highlight Update Failed. PDF exsist');</script>";
            }
        }
        echo "<script type='text/javascript'>window.location='financial-highlight'</script>";
    }
}else{
    header("location:index");
}
?>

<body>
    <div class="wrapper ">
        <!-- SideBar -->
        <?php include 'include/sidebar.php' ?>
        <!-- SideBar -->

        <div class="main-panel">
            <!-- NavBar -->
            <?php include 'include/navbar.php' ?>
            <!-- NavBar -->

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card ">
                                <div class="card-header ">
                                    <h5 class="card-title">FINANCIAL HIGHLIGHT</h5>
                                    <!-- <p class="card-category">24 Hours performance</p> -->
                                </div>
                                <?php foreach($dataEdit as $dt) { ?>
                                <div class="card-body ">
                                    <form action="" method="post" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input type="text" class="form-control form-control-user" name="judul" id="exampleJudul" placeholder="Title" value="<?php echo $dt['Judul']?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Year</label>
                                                    <input type="number" class="form-control form-control-user" name="tahun" id="exampleTahun" placeholder="Year" value="<?php echo $dt['Tahun']?>">
                                                </div>
                                            </div> 
                                        </div>  
                                        <!-- <label for="file">Select a Image:</label>
                                            <input type="file" id="file" name="file"><br><br> -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Stored File : <?php echo $dt['PDF']?></label>
                                                        <input type="file" id="file2" name="file2">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <textarea rows="4" col="80" class="form-control" name="desc" id="exampleDesc" placeholder="Description"><?php echo $dt['Des']?></textarea>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <hr>
                                        <div class="card-footer ">
                                            <div class="stats">
                                                <button name="editFinH" class="btn btn-info btn-fill pull-right"> Edit Financial Highlight</button>
                                            </div>
                                        </form>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </form>
            <!-- Footer -->
            <?php include 'include/footer.php' ?>
            <!-- Footer -->
        </div>
    </div>
    <!-- Script -->
    <?php include 'include/script.php' ?>
    <!-- Script -->
</body>

</html>
