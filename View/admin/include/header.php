<?php
define('PROJECT_ROOT_PATH', __DIR__);
define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);
session_start();

include_once (PROJECT_ROOT_PATH . '../../../../Controller/laporanController.php');
include_once (PROJECT_ROOT_PATH . '../../../../Controller/financialHighlightController.php');  
include_once (PROJECT_ROOT_PATH . '../../../../Controller/financialStatementController.php'); 
include_once (PROJECT_ROOT_PATH . '../../../../Controller/userController.php');
$user = new userController();
$laporan = new laporanController();
$financialHighlight = new financialHighlightController(); ;
$financialStatement = new financialStatementController();
date_default_timezone_set('Asia/Jakarta');
$date = date('Y-m-d H:i:s'); 

if (isset($_POST['submit'])) {
	$login = $user->check_login($_POST['username'], $_POST['password']);
	if ($login == true) { 
		header("location:report");
	} else {
		header("location:index");
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Admin</title>
	<link rel="icon" type="image/x-icon" href="./../assets/img/favicon.ico" />

	<!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quantico:400,700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet"> 
	<!-- CSS Files -->
	<link rel="stylesheet" href="./../assets/icofont/icofont.min.css">
	<link rel="stylesheet" href="./assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="./assets/css/light-bootstrap-dashboard.css?v=2.0.0"/>
</head> 