<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->
    <?php include 'include/navbar.php' ?>
    <!-- Navbar -->

    <!-- Cta Section Begin -->
    <section class="cta-section spad set-bg" data-setbg="img/Corporate-Structure-page-title.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-text">
                        <h2>Publications</h2>
                        <p>Newsroom</p>
                        <!-- <a href="#" class="primary-btn">Contact us</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cta Section End -->

    <!-- Testimoial Section Begin -->
    <section class="testimonial-section">
        <div class="container">
            <div class="row">  
                <?php foreach($data as $dt) { ?>
                <div style="padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-6 col-sm-6">
                    <div class="fs-about">
                        <div class="fa-logo2">
                            <img class="mx-auto" src="<?php echo "admin/assets/img/Upload/".$dt['Image']; ?>">
                            <?php if($dt['Judul'] == "") { ?>
                                <h6> - </h6>
                            <?php } else { ?>
                                <h6> <a href="<?php echo "admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank"><?php echo $dt['Judul']; ?></a></h6>
                            <?php } ?>
                            <?php if($dt['Tahun'] == 0) { ?>
                                <p class="text-muted"> - </p>
                            <?php } else { ?>
                                <p class="text-muted"><?php echo $dt['Tahun']; ?></p>
                            <?php } ?>
                            <?php if($dt['Des'] == "") { ?>
                                <p class="text-muted"> - </p>
                            <?php } else { ?>
                                <p class="text-muted"><?php echo $dt['Des']; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php $no++; } ?>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

        <!-- Footer -->
        <?php include 'include/footer.php' ?>
        <!-- Footer -->
    </body>

    </html>