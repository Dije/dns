<script async src="https://cse.google.com/cse.js?cx=caf30d28032f80ffc"></script>
<header class="header-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo">
                    <a href="./">
                        <img src="../img/logo.png" alt="">
                    </a>
                </div>
                <nav class="nav-menu mobile-menu">
                    <ul class="nav">
                        <li class="active"><a href="./">Beranda</a></li>
                        <li><a href="./sekilas-perusahaan">Tentang Kami</a>
                            <ul class="dropdown" role="menu">
                                <li><a href="./sekilas-perusahaan">Sekilas Perusahaan</a></li>
                                <li><a href="./perjalanan-perusahaan">Perjalanan Perusahaan</a></li>
                                <li><a href="./dewan-direksi">Dewan Direksi</a></li>
                                <li><a href="./dewan-komisaris">Dewan Komisaris</a></li>
                                <li><a href="./struktur-perusahaan">Struktur Perusahaan</a></li>
                                <li><a href="./organisasi-perusahaan">Organisasi Perusahaan</a></li>
                            </ul>
                        </li>
                        <li><a href="./perdagangan-batu-bara">Bisnis Kami</a>
                            <!-- <ul class="dropdown">
                                <li><a href="./coal-trading">Perdaganan Batu Bara</a></li>
                                <li><a href="./coal-shipping">Pengiriman Batu Bara</a></li> 
                            </ul> -->
                        </li>   
                        <li><a href="./keberlangsungan">Keberlangsungan</a>
                            <ul class="dropdown">
                                <li><a href="./kontrol-kualitas">Kontrol Kualitas</a></li>
                                <li><a href="./kesehatan-&-keamanan">Kesehatan & keamanan</a></li>
                                <li><a href="./masalah-lingkungan">Masalah Lingkungan</a></li>
                                <li><a href="./tanggung-jawab-sosial-perusahaan">Tanggung Jawab Sosial Perusahaan</a></li>
                            </ul>
                        </li>
                        <li><a style="cursor: pointer;">Investor</a>
                            <ul class="dropdown">
                                <!-- <li><a href="./financial-summary">Ringkasan Keuangan</a></li> -->
                                <!-- <li><a href="./financial-report">Laporan Keuangan</a></li>
                                    <li><a href="./publications">Publikasi</a></li> -->
                                    <li><a href="./informasi-keuangan">Informasi Keuangan</a></li>
                                    <li><a href="./rups">RUPS</a></li>
                                    <li><a href="./informasi-saham">Informasi Saham</a></li>
                                    <li><a href="./kepemilikan-saham">Kepemilikan Saham</a></li>
                                    <li><a href="./keterbukaan-informasi">Keterbukaan Informasi</a></li>
                                </ul>
                            </li>
                            <!-- <li><a href="./newsroom">Berita</a></li> -->
                            <li><a href="./hubungi-kami">Hubungi Kami</a></li>
                            <li class="lang"> 
                                <a style="cursor: none;color: #FFA400;border: 3px solid #651C32;border-top-left-radius: 11px;border-bottom-left-radius: 11px;height: 0px;padding-bottom: 22px;background: #651C32;opacity: 1;">ID</a>  
                                <a href="../" style="cursor: pointer; padding-left: 0;">EN</a>
                            </li>   
                            <div class="gcse-searchbox-only"></div>
                        </ul>
                    </nav>
                    <div id="mobile-menu-wrap"></div>
                </div>
            </div>
        </div>
    </header>