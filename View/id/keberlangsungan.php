<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/Sustainability-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Keberlangsungan</h2>
						<p><br /></p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section set-bg" data-setbg="../img/Subbar-Business-Overview.png">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title quotes">
						<h4>Kami berusaha keras untuk mencapai pertumbuhan secara berkelanjutan dan menjadi warga perusahaan yang bertanggung jawab terhadap komunitas dan lingkungan yang lebih luas.</h4>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"> 
						<p>Sebagai Grup, kami menyadari pentingnya menempatkan keberlanjutan di jantung strategi bisnis kami. Dalam semua yang kami lakukan, kami memiliki tanggung jawab terhadap masyarakat, staf, pemegang saham, dan lingkungan kami.

						Yang tidak kalah pentingnya adalah komitmen kami dalam memastikan lingkungan kerja yang aman dan sehat bagi staf kami - aset utama dalam mendorong kesuksesan bisnis kami. Investasi kami dalam pelatihan memastikan bahwa karyawan kami dilengkapi dengan keterampilan dan pengetahuan yang diperlukan, dan sertifikasi yang relevan untuk menjalankan tugas mereka.

						Di bidang perlindungan lingkungan, sementara kegiatan operasional kami tidak menghasilkan polutan industri atau limbah berbahaya dan beracun, awak kapal kami memenuhi persyaratan konstruksi dan peralatan terkait peraturan pencegahan pencemaran, serta kepatuhan terhadap peraturan anti-dumping yang relevan di Indonesia.
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>