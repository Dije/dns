<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->
    <?php include 'include/navbar.php' ?>
    <!-- Navbar -->

    <!-- Cta Section Begin -->
    <section class="cta-section spad set-bg" data-setbg="img/growth-page-title.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-text">
                        <h2>Financial Summary</h2>
                        <p>Investors</p>
                        <!-- <a href="#" class="primary-btn">Contact us</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cta Section End -->

    <!-- Testimoial Section Begin -->
    <section class="testimonial-section">
        <div class="container">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <caption style="white-space: pre-line;">Note:
                    (1) For comparative purposes, the earnings per share and the NAV per share were computed based on the share capital of 90,000,000 shares upon the listing of the Company.</caption>
                    <thead>
                      <br />
                        <tr>
                            <th scope="col" rowspan="2" >Financial Year End 31 Dec (SGD'000)</th>
                            <th scope="col" colspan="4" style="text-align: center;" >Audited</th>
                        </tr>
                        <tr>
                            <th scope="col" >FY2016</th>
                            <th scope="col" >FY2017</th>
                            <th scope="col" >FY2018</th>
                            <th scope="col" >FY2019</th>
                        </tr>
                        <tr>
                            <th scope="col" colspan="5" class="bg-warning">Profit and Loss Statement</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>  
                          <th scope="row" >Revenue</th>
                          <td>18,660</td>
                          <td>19,205</td>
                          <td>44,757</td>
                          <td>84,425</td>
                        </tr>
                        <tr>
                          <th scope="row" >Profit before tax</th>
                          <td>4,507</td>
                          <td>2,615</td>
                          <td>4,052</td>
                          <td>1,827</td>
                        </tr>
                        <tr>
                          <th scope="row" >Profit attributable to equity holders of the Company</th>
                          <td>2,099</td>
                          <td>1,203</td>
                          <td>2,242</td>
                          <td>764</td>
                        </tr>
                        <tr>
                          <th scope="row" >Earnings per share <sup>(1)</sup> (cents)</th>
                          <td>2.3</td>
                          <td>1.3</td>
                          <td>2.5</td>
                          <td>0.8</td>
                        </tr>
                        <tr>
                            <th scope="row" colspan="5" class="bg-warning">Balance Sheet (as at 31 Dec)</th>
                        </tr>
                        <tr>  
                          <th scope="row" >Non-current assets</th>
                          <td>16,925</td>
                          <td>14,987</td>
                          <td>14,528</td>
                          <td>35,110</td>
                        </tr>
                        <tr>
                          <th scope="row" >Current assets</th>
                          <td>12,805</td>
                          <td>12,826</td>
                          <td>18,628</td>
                          <td>16,035</td>
                        </tr>
                        <tr>
                          <th scope="row" >Non-current liabilities</th>
                          <td>1,486</td>
                          <td>776</td>
                          <td>284</td>
                          <td>8,222</td>
                        </tr>
                        <tr>
                          <th scope="row" >Current liabilities</th>
                          <td>3,838</td>
                          <td>2,429</td>
                          <td>4,935</td>
                          <td>12,971</td>
                        </tr>
                        <tr>
                          <th scope="row" >Net assets value (NAV) attributable to equity holders of the Company</th>
                          <td>12,018</td>
                          <td>12,143</td>
                          <td>15,499</td>
                          <td>16,800</td>
                        </tr>
                        <tr>
                          <th scope="row" >NAV per share <sup>(1)</sup> (cents)</th>
                          <td>13.4</td>
                          <td>13.5</td>
                          <td>17.2</td>
                          <td>18.7</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Footer -->
    <?php include 'include/footer.php' ?>
    <!-- Footer -->
  </body>

  </html>