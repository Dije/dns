<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/Corporate-Governance-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Corporate Organization</h2>
						<p>About Us</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->


	<section class="testimonial-section">
		<div class="container">
			<div class="row justify-content-center">  
				<button class="button active" onClick="overviewFunction(this)"><a>Overview</a></button>
				<button class="button" onClick="auditFunction(this)"><a>Audit Committee</a></button>
				<button class="button" onClick="remunerationFunction(this)"><a>Remuneration Committee</a></button>
				<button class="button" onClick="nominatingFunction(this)"><a>Nominating Committee</a></button>
				<button class="button" onClick="boardFunction(this)"><a>Corporate Secretary</a></button>  
			</div>
		</div>
	</section>
 
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section set-bg" data-setbg="img/sub-img.png">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Overview"> 
						<p>Our Directors recognise the importance of corporate governance and the offering of high standards of accountability to our Shareholders.

							Our Board has formed three (3) committees: (i) the Nominating Committee; (ii) the Remuneration Committee; and (iii) the Audit Committee. In addition, we have appointed Mr Gouw Eng Seng as our Independent Chairman.

							Our Board comprises five (5) Directors, of which three (3) are Independent Directors. Our Independent Directors do not have any past or existing business or professional relationship of a material nature with our Group, our other Directors and/or Substantial Shareholders. Our Independent Directors are also not related to our other Directors and/or Substantial Shareholders.

						Our Board will hold at least two (2) meetings every year, with additional meetings for particular matters convened when necessary. Our Board will, at least annually, review the adequacy and effectiveness of our Group’s risk management and internal control systems.</p>
					</div>  
				</div>
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Audit" style="display: none;">
						<p>Our Audit Committee comprises Mr Hew Koon Chan, Mr Gouw Eng Seng and Ms Alice Yan. The Chairman of our Audit Committee is Mr Hew Koon Chan.

							Our Audit Committee will assist our Board in discharging their responsibility to safeguard our assets, maintain adequate accounting records and develop and maintain effective systems of internal control, with the overall objective of ensuring that our management creates and maintains an effective control environment in our Group.

							Our Audit Committee will provide a channel of communication between our Board of Directors, our management and our external auditors on matters relating to audit.

						Our Audit Committee will meet periodically and will, amongst others, perform the following key functions:</p>
						<ol type="i" style="padding-left: 30px; font-weight: 700;">
							<li> <p> assist our Board in the discharge of its responsibilities on financial and reporting matters;</li>
							<li> <p> review with the internal auditors the internal audit plans (including scope) and their evaluation of the adequacy of our internal controls, risk management framework and accounting system;</li>
							<li> <p> monitor the implementation of rectification measures proposed by the internal auditors; </li>
							<li> <p> review and report to our Board, at least annually, the adequacy and effectiveness of our Group’s internal controls  and procedures addressing financial, operational, compliance and information technology risks;</li>
							<li> <p> review the relevant policy and procedures, and the scope and adequacy thereof, in respect to the Group’s ongoing compliance with the requirements of the IUP-OPK; </li>
							<li> <p> review the independence and objectivity of the external auditors and recommend their appointment or re-appointment, remuneration and terms of engagement; </li>
							<li> <p> review our Group’s compliance with such functions and duties as may be required under the relevant statutes or the Catalist Rules, including such amendments made thereto from time to time; </li>
							<li> <p> review potential conflicts of interests (if any) and to set out a framework to resolve or mitigate any potential conflicts of interest, and to propose additional measures where appropriate;</li>
							<li> <p> assess and supervise the Company’s, PT DIS’ and PT KNG’s ongoing compliance with the terms set out in the PT DPAL Shareholders’ Agreement; </li>
							<li> <p> review our key financial risk areas, with a view to providing an independent oversight on our Group’s financial reporting; </li>
							<li> <p> review and approve all hedging policies and instruments implemented by our Group and conduct periodic review of foreign exchange transactions and hedging policies and procedures; </li>
							<li> <p> review arrangements by which our staff may, in confidence, raise concerns about possible improprieties in matters of financial reporting and to ensure that arrangements are in place for the independent investigations of such matter and for appropriate follow-up; and </li> </p>
						</ol>
						<p>Apart from the duties listed above, our Audit Committee shall commission and review the findings of internal investigations into matters where there is any suspected fraud or irregularity, or failure of internal controls or suspected infringement of any law, rule or regulation of the jurisdictions in which our Group operates, which has or is likely to have a material impact on our Company’s operating results and/or financial position. In the event that a member of our Audit Committee is interested in any matter being considered by our Audit Committee, he will abstain from reviewing and deliberating on that particular transaction or voting on that particular resolution.</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Remuneration" style="display: none;">
						<p>Our Remuneration Committee comprises Ms Alice Yan, Mr Gouw Eng Seng and Mr Hew Koon Chan. The Chairman of our Remuneration Committee is Ms Alice Yan.

							Our Remuneration Committee will, amongst others, recommend to our Board a framework of remuneration for our Directors and Executive Officers, and determine specific remuneration packages for each Executive Director and Executive Officer. All aspects of remuneration for the aforementioned individuals, including but not limited to directors’ fees, salaries, allowances, bonuses, share option schemes, share-based incentives and awards and other benefits-in-kind shall be review by our Remuneration Committee. Each member of our Remuneration Committee shall abstain from voting on any resolutions in respect of his remuneration package.

						The remuneration of employees related to Directors, Executive Officers and Controlling Shareholders will be reviewed annually by our Remuneration Committee to ensure that their remuneration packages are in line with our staff remuneration guidelines and commensurate with their respective job scopes and level of responsibilities. Any bonuses, pay increases and/or promotions for these related employees will also be subject to the review and approval of our Remuneration Committee. In the event that a member of our Remuneration Committee is related to the employee under review, he will abstain from participating in the review.</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Nominating" style="display: none;">
						<p>Our Nominating Committee comprises Mr Gouw Eng Seng, Ms Alice Yan and Mr Hew Koon Chan. The Chairman of our Nominating Committee is Mr Gouw Eng Seng.

							Our Nominating Committee is responsible for, amongst others, reviewing and recommending board appointments (including the appointment and termination of the members of the board of directors and board of commissioners of PT DPAL), determining the independence of a Director, as well as whether each Director is able to and has been adequately carrying out his duties as a Director, and reviewing potential conflicts of interests in respect of each member of the Board.

						In addition, our Board will also implement a process to be carried out by our Nominating Committee for assessing the effectiveness of our Board as a whole and for assessing the contribution of each individual Director to the effectiveness of our Board. Each member of our Nominating Committee shall abstain from voting on any resolutions in respect of the assessment of his performance or re-nomination as Director. In the event that any member of our Nominating Committee has an interest in a matter being deliberated upon by our Nominating Committee, he will abstain from participating in the review and approval process relating to that matter.</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Board" style="display: none;">
						<p>Our Directors are appointed by our Shareholders at a general meeting, and an election of our Directors takes place annually. One third (or the number nearest one third) of our Directors, are required to retire from office at each AGM. Further, all our Directors are required to retire from office at least once every three (3) years. However, a retiring Director is eligible for re-election at the meeting at which he retires.</p>
					</div>
				</div>  
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>