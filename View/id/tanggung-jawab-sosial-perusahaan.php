<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/csr-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Tanggung Jawab Sosial Perusahaan</h2>
						<p>Keberlangsungan</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12"> 
					<div class="about-text">
					<div class="section-title"> 
						<p>Grup mendukung komunitas lokal di sekitar kita. Dari waktu ke waktu, komunitas lokal juga memanfaatkan tempat kami di Jakarta Utara untuk mengadakan acara komunitas dan kumpul-kumpul sosial.

						Ke depan, inisiatif tanggung jawab sosial dan perusahaan (CSR) kami akan terus berkembang seiring dengan pengelolaan bisnis dan aktivitas operasional kami.

						Untuk meningkatkan inisiatif CSR kami, kami akan terus menilai dan mengelola dampak operasi kami terhadap lingkungan dan pemangku kepentingan kami, termasuk masyarakat lokal, pelanggan, karyawan, pemasok, dan pemegang saham kami.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>