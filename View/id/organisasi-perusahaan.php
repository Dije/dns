<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/Corporate-Governance-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Organisasi Perusahaan</h2>
						<p>Tentang Kami</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->


	<section class="testimonial-section">
		<div class="container">
			<div class="row justify-content-center">  
				<button class="button active" onClick="overviewFunction(this)"><a>Gambaran Umum</a></button>
				<button class="button" onClick="auditFunction(this)"><a>Komite Audit</a></button>
				<button class="button" onClick="remunerationFunction(this)"><a>Komite Remunerasi</a></button>
				<button class="button" onClick="nominatingFunction(this)"><a>Komite Nominasi</a></button>
				<button class="button" onClick="boardFunction(this)"><a>Sekretaris Perusahaan</a></button>  
			</div>
		</div>
	</section>
 
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section set-bg" data-setbg="../img/sub-img.png">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Overview"> 
						<p>Direksi kami menyadari pentingnya tata kelola perusahaan dan menawarkan standar akuntabilitas yang tinggi kepada Pemegang Saham kami.

							Dewan kami telah membentuk tiga (3) komite: (i) Komite Nominasi; (ii) Komite Remunerasi; dan (iii) Komite Audit. Selain itu, kami telah menunjuk Tuan Gouw Eng Seng sebagai Ketua Independen kami.

							Dewan kami terdiri dari lima (5) Direktur, yang mana tiga (3) adalah Direktur Independen. Direktur Independen kami tidak memiliki hubungan masa lalu atau hubungan bisnis atau hubungan profesional yang bersifat material dengan Grup kita, Direktur kita yang lain, dan/atau Pemegang Saham Utama. Direktur Independen kami juga tidak memiliki hubungan dengan Direktur lain dan/atau Pemegang Saham Utama. 

						Dewan kami akan mengadakan setidaknya dua (2) pertemuan setiap tahun, dengan pertemuan tambahan untuk hal-hal tertentu yang diadakan bila diperlukan. Dewan kita akan, setidaknya setiap tahun, meninjau kecukupan dan efektivitas sistem manajemen risiko dan pengendalian internal Grup kita.</p>
					</div>  
				</div>
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Audit" style="display: none;">
						<p>Komite Audit kami terdiri dari Tuan Hew Koon Chan, Tuan Gouw Eng Seng dan Nyonya Alice Yan. Ketua Komite Audit kami adalah Tuan Hew Koon Chan.

							Komite Audit kita akan membantu Dewan kita dalam melaksanakan tanggung jawab mereka untuk melindungi aset kita, memelihara catatan akuntansi yang memadai dan mengembangkan serta memelihara sistem pengendalian internal yang efektif, dengan tujuan keseluruhan untuk memastikan bahwa manajemen kita menciptakan dan memelihara lingkungan pengendalian yang efektif di Grup kita.

							Komite Audit kami akan menyediakan saluran komunikasi antara Dewan Direksi kami, manajemen kami dan auditor eksternal kami mengenai hal-hal yang berkaitan dengan audit.

						Komite Audit kami akan bertemu secara berkala dan akan, antara lain, menjalankan fungsi-fungsi utama berikut ini:</p>
						<ol type="i" style="padding-left: 30px; font-weight: 700;">
							<li> <p> Membantu Dewan kami dalam melaksanakan tanggung jawabnya pada masalah keuangan dan pelaporan;</li>
							<li> <p> Meninjau dengan auditor internal rencana audit internal (termasuk ruang lingkup) dan evaluasi mereka terhadap kecukupan pengendalian internal, kerangka kerja manajemen risiko dan sistem akuntansi kami;</li>
							<li> <p> Memantau pelaksanaan langkah-langkah perbaikan yang diusulkan oleh auditor internal;</li>
							<li> <p> Meninjau dan melaporkan kepada Dewan kami, setidaknya setiap tahun, kecukupan dan efektivitas pengendalian dan prosedur internal Grup yang menangani risiko keuangan, operasional, kepatuhan, dan teknologi informasi;</li>
							<li> <p> Meninjau kebijakan dan prosedur yang relevan, serta ruang lingkup dan kecukupannya, sehubungan dengan kepatuhan Grup yang berkelanjutan terhadap persyaratan IUP-OPK; </li>
							<li> <p> Meninjau independensi dan objektivitas auditor eksternal dan merekomendasikan pengangkatan atau penunjukan kembali mereka, remunerasi dan persyaratan penugasan; </li>
							<li> <p> Meninjau kepatuhan Grup kita dengan fungsi dan tugas yang mungkin diperlukan berdasarkan undang-undang yang relevan atau Aturan Catalist, termasuk amandemen yang dibuat darinya dari waktu ke waktu; </li>
							<li> <p> Meninjau potensi konflik kepentingan (jika ada) dan untuk menetapkan kerangka kerja untuk menyelesaikan atau mengurangi potensi konflik kepentingan, dan untuk mengusulkan tindakan tambahan jika sesuai;</li>
							<li> <p> Menilai dan mengawasi kepatuhan Perusahaan, PT DIS 'dan PT KNG yang sedang berlangsung dengan ketentuan yang diatur dalam Perjanjian Pemegang Saham PT DPAL; </li>
							<li> <p> Meninjau area risiko keuangan utama kita, dengan tujuan untuk memberikan pengawasan independen pada pelaporan keuangan Grup kita; </li>
							<li> <p> Meninjau dan menyetujui semua kebijakan dan instrumen lindung nilai yang diterapkan oleh Grup kita dan melakukan peninjauan berkala atas transaksi valuta asing serta kebijakan dan prosedur lindung nilai; </li>
							<li> <p> Meninjau pengaturan di mana staf kami dapat, secara rahasia, menyampaikan kekhawatiran tentang kemungkinan ketidakwajaran dalam masalah pelaporan keuangan dan untuk memastikan bahwa pengaturan tersedia untuk penyelidikan independen atas masalah tersebut dan untuk tindak lanjut yang sesuai; dan</li> </p>
						</ol>
						<br />
						<p>Terlepas dari tugas-tugas yang disebutkan di atas, Komite Audit kami akan menugaskan dan meninjau temuan-temuan dari penyelidikan internal mengenai hal-hal di mana terdapat kecurigaan adanya kecurangan atau penyimpangan, atau kegagalan pengendalian internal atau dugaan pelanggaran hukum, aturan atau peraturan yurisdiksi di mana Grup kita beroperasi, yang memiliki atau kemungkinan besar berdampak material pada hasil operasi dan/atau posisi keuangan Perusahaan kita. Jika salah satu anggota Komite Audit kami tertarik dengan masalah apa pun yang sedang dipertimbangkan oleh Komite Audit kami, ia akan abstain dari meninjau dan mempertimbangkan transaksi tertentu atau memberikan suara pada resolusi tersebut.</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Remuneration" style="display: none;">
						<p>Komite Remunerasi kami terdiri dari Ibu Alice Yan, Bapak Gouw Eng Seng dan Bapak Hew Koon Chan. Ketua Komite Remunerasi kami adalah Ibu Alice Yan.

							Komite Remunerasi kami akan, antara lain, merekomendasikan kepada Dewan kami kerangka kerja remunerasi untuk Direktur dan Pejabat Eksekutif kami, dan menentukan paket remunerasi khusus untuk setiap Direktur Eksekutif dan Pejabat Eksekutif. Semua aspek remunerasi untuk individu yang disebutkan di atas, termasuk namun tidak terbatas pada biaya direktur, gaji, tunjangan, bonus, skema opsi saham, insentif dan penghargaan berbasis saham, dan manfaat sejenis lainnya akan ditinjau oleh Komite Remunerasi kami. Setiap anggota Komite Remunerasi kami harus abstain dari pemungutan suara pada setiap resolusi sehubungan dengan paket remunerasinya.

						Remunerasi karyawan yang terkait dengan Direktur, Pejabat Eksekutif, dan Pemegang Saham Pengendali akan ditinjau setiap tahun oleh Komite Remunerasi kami untuk memastikan bahwa paket remunerasi mereka sejalan dengan pedoman remunerasi staf kami dan sesuai dengan ruang lingkup pekerjaan dan tingkat tanggung jawab masing-masing. Setiap bonus, kenaikan gaji dan/atau promosi untuk karyawan terkait ini juga akan ditinjau dan disetujui oleh Komite Remunerasi kami. Dalam hal anggota Komite Remunerasi terkait dengan karyawan yang ditinjau, ia tidak akan berpartisipasi dalam peninjauan tersebut.</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Nominating" style="display: none;">
						<p>Komite Nominasi kami terdiri dari Mr Gouw Eng Seng, Ms Alice Yan dan Mr Hew Koon Chan. Ketua Komite Nominasi kami adalah Tuan Gouw Eng Seng.

							Komite Nominasi kami bertanggung jawab untuk, antara lain, meninjau dan merekomendasikan pengangkatan dewan (termasuk pengangkatan dan pemberhentian anggota direksi dan dewan komisaris di PT DPAL), menentukan independensi seorang Direktur, serta apakah masing-masing Direktur mampu dan telah menjalankan tugasnya secara memadai sebagai Direktur, dan mengkaji potensi benturan kepentingan masing-masing anggota Direksi.

						Selain itu, Dewan kami juga akan menerapkan proses yang akan dilakukan oleh Komite Nominasi kami untuk menilai efektivitas Dewan kami secara keseluruhan dan untuk menilai kontribusi masing-masing Direktur terhadap efektivitas Dewan kami. Setiap anggota Komite Nominasi kami tidak boleh memberikan suara pada keputusan apa pun sehubungan dengan penilaian kinerjanya atau pencalonan kembali sebagai Direktur. Jika ada anggota Komite Nominasi kami memiliki kepentingan dalam suatu masalah yang sedang dibahas oleh Komite Nominasi kami, dia akan abstain dari berpartisipasi dalam proses peninjauan dan persetujuan terkait dengan masalah tersebut.</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Board" style="display: none;">
						<p>Direktur kami diangkat oleh Pemegang Saham kami pada rapat umum, dan pemilihan Direktur kami dilakukan setiap tahun. Sepertiga (atau nomor terdekat sepertiga) dari Direktur kami, diharuskan untuk pensiun dari jabatannya pada setiap RUPS. Selain itu, semua Direktur kami diwajibkan untuk pensiun dari jabatannya setidaknya sekali setiap tiga (3) tahun. Namun, seorang direktur yang pensiun berhak untuk dipilih kembali pada rapat di mana dia pensiun.</p>
					</div>
				</div>  
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>