<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<?php 
$dataAu = $audit->getData(); 
$year = array();
$Rev = array();  
$ProBfTx = array();
$ProAtt = array();
$EarnPS = array();
$NCurrAsset = array();
$CurrAsset = array();
$NCurrLia = array();
$CurrLia = array();
$AttEqui = array();
$NavPerSh = array();
foreach ($dataAu as $dr ) {
	$yr = array($dr['Year']);
	$revenue = (array(array("y"=>$dr['Revenue']))); 
	$ProfBfTax = (array(array("y"=>$dr['ProfitBfTax']))); 
	$profatt = (array(array("y"=>$dr['ProfitAttributable']))); 
	$earnps = (array(array("y"=>$dr['EarningsPerShare']))); 
	$ncurrasset = (array(array("y"=>$dr['NonCurrentAsset']))); 
	$currasset = (array(array("y"=>$dr['CurrentAssets']))); 
	$ncurrlia = (array(array("y"=>$dr['NonCurrentLia']))); 
	$currlia = (array(array("y"=>$dr['CurrentLia']))); 
	$attequi = (array(array("y"=>$dr['AttEquity']))); 
	$navpersh = (array(array("y"=>$dr['NavPerShare']))); 
	$year = array_merge($year,$yr) ;
	$Rev = array_merge($Rev,$revenue); 
	$ProBfTx = array_merge($ProBfTx,$ProfBfTax); 
	$ProAtt = array_merge($ProAtt,$profatt); 
	$EarnPS = array_merge($EarnPS,$earnps); 
	$NCurrAsset = array_merge($NCurrAsset,$ncurrasset); 
	$CurrAsset = array_merge($CurrAsset,$currasset);  
	$NCurrLia = array_merge($NCurrLia,$ncurrlia);  
	$CurrLia = array_merge($CurrLia,$currlia);  
	$AttEqui = array_merge($AttEqui,$attequi);  
	$NavPerSh = array_merge($NavPerSh,$navpersh);  
}   
 
$Year = $year;
$Revenue = $Rev;
$ProfitBfTax = $ProBfTx; 
$ProfitAttributable = $ProAtt;
$EarningsPerShare = $EarnPS;
$NonCurrentAsset = $NCurrAsset;
$CurrentAssets = $CurrAsset;
$NonCurrentLia = $NCurrLia;
$CurrentLia = $CurrLia;
$AttEquity = $AttEqui;
$NavPerShare = $NavPerSh; 
?>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/Corporate-Governance-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Company Report</h2>
						<p>Investors</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container"> 
			<div class="row justify-content-center">  
				<button class="button active" onClick="overviewFunction(this)"><a>Overview</a></button>
				<button class="button" onClick="auditFunction(this)"><a>Annual Reports</a></button>
				<button class="button" onClick="remunerationFunction(this)"><a>Financial Statements</a></button>
				<button class="button" onClick="nominatingFunction(this)"><a>Financial Highlight</a></button> 
			</div>
		</div>
	</section>
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Overview"> 
						<p>The latest quarterly results, dividend information and presentations for DNS Companies are published on this site. Although the documents contained on this site have been selected and are in the interest of our current and future shareholders and equity research analysts, some of the matters discussed on this site may contain statements based on forecasts made at that time. , which contains risks and uncertainties that could result in material different from those expressed or implied in such statements.</p> 
						<figure class="highcharts-figure">
							<div id="container"></div>
						</figure>    
						
							<p class="highcharts-description">
								Note:
								(1) For comparative purposes, the earnings per share and the NAV per share were computed based on the share capital of 90,000,000 shares upon the listing of the Company.
							</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Audit" style="display: none;">
						<p><b>One of the fastest growing coal producers in Indonesia.</b>
						Since the purchase of its first mining concession by shareholders in 1998, DNS has shown rapid growth to date, having 5 Coal Mining Concession Work Agreements (PKB2B) and 16 Mining Business Permits (IUP) with a total concession area of 126,293 ha. DNS has large coal reserves and resources and is one of the best reputable coal suppliers in Indonesia. DNS's coal products are environmentally friendly coal that is managed in an integrated manner, from mining plans to shipping coal to consumers. Production quality control and focus on customers is one of the DNS's strengths.</p>
						<br>
						<div class="container">
							<div class="row">  
								<div class="col-sm-2">
									<p style="float: right;">Year:</p>
								</div>
								<div class="col-sm-10">
									<select onchange="searchFunction()" id="myInput" class="col-sm-4 form-control form-control-sm">
										<option value="" selected>All</option>
										<option value="2021">2021</option>
										<option value="2020">2020</option>
										<option value="2019">2019</option>
										<option value="2018">2018</option>
									</select>
									<br>
								</div>
								<table id="myTable" class="table">
									<tbody><?php foreach($data as $dt) { ?>
										<tr>
											<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="img/file.png"></td>
											<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
											<td style="text-align: center;"><a class="download" href="<?php echo "admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
											<td hidden><?php echo $dt['Tahun']; ?></td>
										</tr> 
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>  
				</div>
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Remuneration" style="display: none;">
						<p><b>The growth of the Company's coal production over the past 10 years</b> has experienced ups and downs in line with developments in the coal market and the time required to build the necessary infrastructure in Tabang. Since 2015, the Company has focused on expanding Tabang, and in 2017, the Company managed to significantly increase production to 20.9 MT, or an increase of 115% from the previous year. This increase is in line with our strategy to expand Tabang to become one of the largest coal mines in Indonesia. The company also predicts that the increase in production will continue into 2018, with a production target of between 24 and 28 million tonnes of coal.</p>
						<br>
						<div class="container">
							<div class="row">  
								<div class="col-sm-2">
									<p style="float: right;">Year:</p>
								</div>
								<div class="col-sm-10">
									<select onchange="searchFunction1()" id="myInput1" class="col-sm-4 form-control form-control-sm">
										<option value="" selected>All</option>
										<option value="2021">2021</option>
										<option value="2020">2020</option>
										<option value="2019">2019</option>
										<option value="2018">2018</option>
									</select>
									<br>
								</div>
								<table id="myTable1" class="table">
									<tbody><?php foreach($dataFS as $dt) { ?>
										<tr>
											<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="img/file.png"></td>
											<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
											<td style="text-align: center;"><a class="download" href="<?php echo "admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
											<td hidden><?php echo $dt['Tahun']; ?></td>
										</tr> 
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>  
				</div>
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Nominating" style="display: none;">
						<p>In 2017, the Company achieved revenues of US $ 1,067.4 million above the target of $ 717.5 million, exceeding the target of 48.8%. This significant increase in revenue was the culmination of a strong coal market, which led to higher coal selling prices (Average Selling Price (ASP) was US $ 52.1 per tonne, higher than the target of US $ 40.2 per metric. tonnes) and coal sales that were higher than the target, with coal selling 20.1 million tonnes or above the target of 17.5 million tonnes. Coupled with a very profitable increase in revenue, the Company was able to lower its cash costs to US $ 29.0 per metric ton, lower than the target of US $ 30.3 per metric ton. The net results of the above are very positive margins, which exceed targets in all aspects, and achievement of profitability margins that rival and in many cases exceed those of the larger and more established mining companies.</p>
						<br>
						<div class="container">
							<div class="row">  
								<div class="col-sm-2">
									<p style="float: right;">Year:</p>
								</div>
								<div class="col-sm-10">
									<select onchange="searchFunction2()" id="myInput2" class="col-sm-4 form-control form-control-sm">
										<option value="" selected>All</option>
										<option value="2021">2021</option>
										<option value="2020">2020</option>
										<option value="2019">2019</option>
										<option value="2018">2018</option>
									</select>
									<br>
								</div>
								<table id="myTable2" class="table">
									<tbody><?php foreach($dataFH as $dt) { ?>
										<tr>
											<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="img/file.png"></td>
											<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
											<td style="text-align: center;"><a class="download" href="<?php echo "admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
											<td hidden><?php echo $dt['Tahun']; ?></td>
										</tr> 
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>  
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>