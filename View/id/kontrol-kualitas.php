<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/Quality-Control-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Kontrol Kualitas</h2>
						<p>Keberlangsungan</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section set-bg" data-setbg="../img/Subbar-Business-Overview.png">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title quotes">
						<h4>Kami berkomitmen untuk menjaga standar kualitas layanan yang tinggi, yang dipraktikkan di semua tahapan proses bisnis kami untuk Bisnis Perdagangan Batubara kami. Proses yang ketat dan kontrol kualitas diterapkan untuk mencapai kepuasan pelanggan yang maksimal.</h4>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"> 
						<div class="container">
							<div class="row">
								<div class="col-xs-12 ">
									<nav>
										<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
											<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Bisnis Perdagangan Batubara</a>
											<!-- <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Coal Shipping Service</a> -->
										</div>
									</nav>
									<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
										<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
											<section class="testimonial-section">
												<div class="container">
													<div class="row">
														<div class="col-md-6">
															<div class="base"><img class="img-fluid" 
																src="../assets/img/Quality Control/coal-trading.jpg" alt="" />
															</div>
														</div>
														<div class="col-md-6 about-text">
															<div class="section-title">
															<p class="info">Setiap pengiriman batubara harus dievaluasi secara independen oleh surveyor independen yang disepakati bersama dengan pelanggan kami. Surveyor memeriksa, mengevaluasi dan memberikan laporan tentang kualitas dan spesifikasi batubara yang akan dikirim, untuk memastikan bahwa batubara tersebut memenuhi persyaratan pelanggan kami.

															Kami juga memiliki prosedur penilaian pelanggan internal untuk mengevaluasi dan menilai reputasi, peringkat kredit, dan ketepatan waktu pembayaran pelanggan potensial.</p>
															</div>
														</div>
													</div>
												</div>
											</section> 	
										</div>
										<!-- <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
											<section class="testimonial-section">
												<div class="container">
													<div class="about-text"> 
													<div class="section-title">
														<div class="row">
															<div class="col-md-6">
																<div class="timeline-image"><img class="img-fluid" 
																	src="assets/img/Quality Control/coal-shipping.jpg" alt="" />
																</div>
															</div>
															<div class="col-md-6">
																We adopt a policy of continually refurbishing and repairing our fleet where necessary. This will allow us to maintain operational reliability and minimise expenditure on major fleet repairs and maintenance work over the long term.

																Regular inspection and maintenance are also carried out onboard our vessels by the assigned crew, to ensure that the vessels are in good working condition.
															</div>
														</div>  
													</div>
												 </div> -->
												</div>
											</section>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>