<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->
    <?php include 'include/navbar.php' ?>
    <!-- Navbar -->

    <!-- Cta Section Begin -->
    <section class="cta-section spad set-bg" data-setbg="../img/environmental-matters-page-title.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-text">
                        <h2>Masalah Lingkungan</h2>
                        <p>Keberlangsungan</p>
                        <!-- <a href="#" class="primary-btn">Contact us</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cta Section End -->
    
    <!-- Testimoial Section Begin -->
    <section class="testimonial-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12"> 
                    <div class="about-text">
                        <div class="section-title"> 
                            <p>Kami menjalankan semua aspek bisnis kami sesuai dengan hukum lingkungan yang berlaku, dan telah mengadakan sesi pelatihan, kebijakan operasional, dan arahan untuk (i) memastikan bahwa karyawan dan awak kapal kami mematuhi semua hukum dan peraturan lingkungan yang berlaku; dan (ii) mencegah, mengurangi dan mengelola terjadinya pencemaran lingkungan yang berasal dari kapal kami.

                            Kami juga telah memperluas persyaratan lingkungan dan kualitas yang sama ke galangan kapal yang kami gunakan untuk pembangunan kapal dan tongkang baru kami dan pemeliharaan berkelanjutan dari armada kami yang ada. Galangan kapal tersebut dinilai dan dipilih berdasarkan, antara lain, rekam jejak kepatuhan mereka sehubungan dengan undang-undang dan peraturan lingkungan, dan inisiatif yang mereka adopsi tentang perlindungan lingkungan dan praktik keberlanjutan.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Testimonial Section End -->

<!-- Footer -->
<?php include 'include/footer.php' ?>
<!-- Footer -->
</body>

</html>