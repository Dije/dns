<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/health-safety-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Kesehatan & Keamanan</h2>
						<p>Keberlangsungan</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12"> 
					<div class="about-text">
						<div class="section-title"> 
							<p>Prosedur ketat diterapkan untuk memastikan lingkungan kerja yang aman dan sehat bagi staf kami, dan standar serta peraturan kesehatan dan keselamatan yang berlaku terpenuhi.

							Kami juga melakukan audit keselamatan dan lingkungan secara teratur dan memberikan pelatihan kesehatan dan keselamatan yang sistematis untuk semua staf kami, yang mencakup pemeriksaan kesehatan, kebijakan dan prosedur keselamatan, pencegahan dan panduan kecelakaan, serta menjaga lingkungan dari bahan berbahaya dan pembuangan limbah yang tepat.

							Grup bekerja sama dengan harbourmaster dan otoritas pelabuhan terkait untuk sepenuhnya mematuhi peraturan keselamatan lokal, di mana kapal yang berlayar di perairan Indonesia diharuskan mendapatkan sertifikat yang relevan untuk memastikan kelayakan kapal, termasuk keselamatan kapal, keselamatan radio, dan jalur muat.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonial Section End -->

<!-- Footer -->
<?php include 'include/footer.php' ?>
<!-- Footer -->
</body>

</html>