<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/cta-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Peta Situs</h2>
						<p>Mengenai DNS</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Footer Section Begin -->
	<footer class="footer-section">
		<div class="container">
			<!-- <h5>Peta Situs</h5> -->
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6">
				<!-- <div class="fs-about">
					<div class="fa-logo">
						<a href="./">
							<img src="../img/f-logo.png" alt="">
						</a>
					</div>
					<p>Sedikit pengenalan mengenai PT. DNS dapat dilakukan disini.</p>
					<div class="fa-social">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-youtube-play"></i></a>
						<a href="#"><i class="fa fa-instagram"></i></a>
					</div>
				</div>
				<br /> -->
				<div class="fs-widget">
					<ul>
						<li><a href="./"><b>Beranda</b></a></li>
						<br />
						<li><a href="./sekilas-perusahaan"><b>Tentang Kami</b></a></li>
						<li class="sub"><a href="./sekilas-perusahaan">Sekilas Perusahaan</a></li>
						<li class="sub"><a href="./perjalanan-perusahaan">Perjalanan Perusahaan</a></li>
						<li class="sub"><a href="./dewan-direksi">Dewan Direksi</a></li> 
						<li class="sub"><a href="./dewan-komisaris">Dewan Komisaris</a></li>
						<li class="sub"><a href="./struktur-perusahaan">Struktur Perusahaan</a></li>
						<li class="sub"><a href="./organisasi-perusahaan">Organisasi Perusahaan</a></li> 
						<br />
						<li><a href="./perdagangan-batu-bara"><b>Bisnis Kami</b></a></li>
						<li class="sub"><a href="./perdagangan-batu-bara#TradingRiskManagement">Manajemen Risiko Perdagangan</a></li>
						<!-- <li class="sub"><a href="./coal-shipping">Pengiriman Batu Bara</a></li> -->
						<br />                               
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="fs-widget">
					<!-- <h5> <br /> </h5> -->
					<ul>
						<li><a href="./keberlangsungan"><b>Keberlangsungan</b></a></li>
						<li class="sub"><a href="./kontrol-kualitas">Kontrol Kualitas</a></li>
						<li class="sub"><a href="./kesehatan-&-keamanan">Kesehatan & keamanan</a></li>
						<li class="sub"><a href="./masalah-lingkungan">Masalah Lingkungan</a></li>
						<li class="sub"><a href="./tanggung-jawab-sosial-perusahaan">Tanggung Jawab Sosial Perusahaan</a></li>
						<br />
						<li><a style="cursor: none;"><b>Investor</b></a></li>
						<!-- <li class="sub"><a href="./financial-summary">Ringkasan Keuangan</a></li>
						<li class="sub"><a href="./financial-report">Laporan Keuangan</a></li>
						<li class="sub"><a href="./publications">Publikasi</a></li> -->
						<li class="sub"><a href="./informasi-keuangan">Informasi Keuangan</a></li>
                        <li class="sub"><a href="./rups">RUPS</a></li>
                        <li class="sub"><a href="./informasi-saham">Informasi Saham</a></li>
                        <li class="sub"><a href="./kepemilikan-saham">Kepemilikan Saham</a></li>
                        <li class="sub"><a href="./keterbukaan-informasi">Keterbukaan Informasi</a></li>
						<!-- <br />
						  <li><a href="./newsroom"><b>Berita</b></a></li>-->
						<br /> 
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="fs-widget">
					<ul> 
						<li><a href="./hubungi-kami"><b>Hubungi Kami</b></a></li>
						<br />
 						<li><a style="cursor: none;"><b>Bahasa</b></a></li>
                        <li class="sub"><a style="padding-left: 3px; cursor: none; color: #333;"><img width="30px" src="../assets/img/indonesia.png">Indonesia</a><li> 
                        <li class="sub"><a style="padding-left: 3px;" href="../"><img width="30px" src="../assets/img/english.png">English</a><li>
                        	<br /> 
					</ul>
				</div>
			</div>
		</footer>

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer --> 
	</body>

	</html>