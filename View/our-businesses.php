<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/our-business-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Our Businesses</h2>
						<p> <br /> </p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section set-bg" data-setbg="img/Subbar-Business-Overview.png">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title quotes">
						<h4>We operate in Indonesia – one of the leading producers of coal globally whose coal consumption is supported by growth in coal-fired power generation and the government’s plans to raise electrification rate in the country.</h4>
					</div>
				</div>
			</div>
		</section>
		<section class="testimonial-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="timeline-image"><img class="img-fluid" 
							src="assets/img/Our Businesses/coal-trading-map.png" alt="" />
						</div>
					</div>
					<div class="col-md-4 about-text">
						<div class="section-title">
						<p class="detail">Our Coal Trading Business procures coal from coal mines located in South Kalimantan for domestic sales, while our Coal Shipping Services Business covers domestic shipping routes between coal mines located in South Kalimantan and the Java and Sulawesi islands in Indonesia. </p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>