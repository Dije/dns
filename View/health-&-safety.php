<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/health-safety-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Health & Safety</h2>
						<p>Sustainability</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12"> 
					<div class="about-text">
						<div class="section-title"> 
							<p> Stringent procedures are in place to ensure a safe and healthy work environment for our staff, and that prevailing health and safety standards and regulations are met.

								We also conduct regular safety and environmental audits and provide systematic health and safety training for all our staff, covering health checks, safety policy and procedures, accident prevention and guidance as well as safeguarding the environment against any hazardous materials and proper waste disposal.

							The Group works closely with the harbourmaster and the relevant port authorities to fully comply with local safety regulations, where vessels plying the Indonesian waters are required to obtain relevant certificates to ensure seaworthiness of the vessels, including vessel safety, radio safety and load line.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonial Section End -->

<!-- Footer -->
<?php include 'include/footer.php' ?>
<!-- Footer -->
</body>

</html>