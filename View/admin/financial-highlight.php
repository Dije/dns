<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<?php   
$dataFH = $financialHighlight->getData(); 
$no = 1;

if ($user->get_session() == true) {
	if (isset($_POST['addFH'])){
		// $ekstensi_diperbolehkan = array('png','jpg');
		// $nama = $_FILES['file']['name'];
		// $x = explode('.', $nama);
		// $ekstensi = strtolower(end($x));
		// $ukuran = $_FILES['file']['size'];
		// $file_tmp = $_FILES['file']['tmp_name'];
		$title = $_POST['judul'];
		$year = $_POST['tahun'];
		$desc = $_POST['desc'];

		$ekstensi_diperbolehkan2 = array('pdf');
		$nama2 = $_FILES['file2']['name'];
		$y = explode('.', $nama2);
		$ekstensi2 = strtolower(end($y));
		$ukuran2 = $_FILES['file2']['size'];
		$file_tmp2 = $_FILES['file2']['tmp_name'];

		if(in_array($ekstensi2, $ekstensi_diperbolehkan2) === true){
			if($ukuran2 < 150*MB){   
				$add = $financialHighlight->addReport($title, $year, $desc, $nama2, $date);
				if($add){
					// move_uploaded_file($file_tmp, 'assets/img/FinancialHighlight/'.$nama);
					move_uploaded_file($file_tmp2, 'assets/pdf/FinancialHighlight/'.$nama2);
					// chmod('assets/img/FinancialHighlight/'.$nama, 0664);
					chmod('assets/pdf/FinancialHighlight/'.$nama2, 0777);
					echo "<script type='text/javascript'>alert('Financial Highlight Added Success');</script>";
				}else{
					echo "<script type='text/javascript'>alert('Financial Highlight Added Failed. PDF exsist');</script>";
				}
			}else{
				echo "<script type='text/javascript'>alert('File Too Big');</script>";
			}
		}else{
			echo "<script type='text/javascript'>alert('Extension Is Not Allowed');</script>";
		}
		echo "<script type='text/javascript'>window.location='financial-highlight'</script>";
	}
}else{
	header("location:index");
}
?>

<body>
	<div class="wrapper ">
		<!-- SideBar -->
		<?php include 'include/sidebar.php' ?>
		<!-- SideBar -->

		<div class="main-panel">
			<!-- NavBar -->
			<?php include 'include/navbar.php' ?>
			<!-- NavBar -->

			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card ">
								<div class="card-header ">
									<h5 class="card-title">FINANCIAL HIGHLIGHT</h5>
									<!-- <p class="card-category">24 Hours performance</p> -->
								</div>
								<div class="card-body ">
									<form action="" method="post" enctype="multipart/form-data">
										<div class="row">
											<div class="col-md-8">
												<div class="form-group">
													<label>Title</label>
													<input type="text" class="form-control form-control-user" name="judul" id="exampleJudul" placeholder="Title">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Year</label>
													<input type="number" class="form-control form-control-user" name="tahun" id="exampleTahun" placeholder="Year">
												</div>
											</div> 
										</div>  
										<!-- <label for="file">Select a Image:</label>
										<input type="file" id="file" name="file"><br><br> -->
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Select A Report</label>
													<input type="file" id="file2" name="file2">
												</div>
											</div>
										</div>  
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label>Description</label>
													<textarea rows="4" col="80" class="form-control" name="desc" id="exampleDesc" placeholder="Description"></textarea>
												</div>
											</div>
										</div> 
									</div>
									<hr>
									<div class="card-footer ">
										<div class="stats">
											<button name="addFH" class="btn btn-info btn-fill pull-right"> Add Financial Highlight </button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card ">
								<div class="card-header ">
									<h5 class="card-title">FINANCIAL HIGHLIGHT LIST</h5>
									<!-- <p class="card-category">24 Hours performance</p> -->
								</div>
								<div class="card-body ">
									<div class="row">
										<div class="table-responsive">  
											<table id="myTable" class="table table-hover table-dark">
												<thead>
													<tr>
														<th scope="col">No</th>
														<th scope="col">Judul</th>
														<th scope="col">Tahun</th>
														<!-- <th scope="col">Image</th> -->
														<th scope="col">Desc</th>
														<th scope="col">PDF</th>
														<th scope="col">Action</th>
													</tr>
												</thead>

												<tbody>
													<?php 
													if($dataFH=="-"){ ?>
														<tr><td>-</td>
															<td>-</td>
															<td>-</td>
															<!-- <td>-</td> -->
															<td>-</td>
															<td>-</td></tr>
														<?php }else{
															foreach($dataFH as $fh) { ?>
																<tr id="header">
																	<th scope="row"><?php echo $no; ?></th>
																	<td><?php echo $fh['Judul']; ?></td>
																	<td><?php echo $fh['Tahun']; ?></td>
																	<!-- <td><?php //echo $fh['Image']; ?></td> -->
																	<td><?php echo $fh['Des']; ?></td>
																	<td><?php echo $fh['PDF']; ?></td>
																	<td>
																		<a href="editFinancialHighlight?id=<?php echo $fh['ID_Laporan']; ?>" class="btn btn-info btn"> Edit</a>
																		<a href="deleteFinancialHighlight?id=<?php echo $fh['ID_Laporan']; ?>" class="btn btn-danger btn"> Delete</a>
																	</td>
																</tr>
																<?php $no++; }
															}?>
														</tbody>
													</table>
												</div>
												<div class="col-md-12 text-center">
													<ul class="pagination pagination-lg pager" id="myPager"></ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- Footer -->
				<?php include 'include/footer.php' ?>
				<!-- Footer -->
			</div>
		</div>
		<!-- Script -->
		<?php include 'include/script.php' ?>
		<!-- Script -->
	</body>

	</html>
