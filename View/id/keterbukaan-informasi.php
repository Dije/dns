<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/cta-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Keterbukaan Informasi</h2>
						<p>Investor</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<section class="testimonial-section">
		<div class="container">
			<div class="row"> 
				<div class="about-text">
					<div class="section-title"> 
						<div class="container">
							<div class="row">  
								<div class="col-sm-2">
									<p style="float: right;">Tahun:</p>
								</div>
								<div class="col-sm-10">
									<select onchange="searchFunction()" id="myInput" class="col-sm-4 form-control form-control-sm">
										<option value="" selected>All</option>
										<option value="2021">2021</option>
										<option value="2020">2020</option>
										<option value="2019">2019</option>
										<option value="2018">2018</option>
									</select>
									</br>
								</div>
								<table id="myTable" class="table">
									<tbody><?php foreach($data as $dt) { ?>
										<tr>
											<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="../img/file.png"></td>
											<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
											<td style="text-align: center;"><a class="download" href="<?php echo "../admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
											<td hidden><?php echo $dt['Tahun']; ?></td>
										</tr> 
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>  
				</div> 
			</div>
		</div>
	</section>

	<!-- Testimoial Section Begin -->
	<!-- <section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"> 
                        <p>Our history of being involved in the coal industry can be traced back to around 2005 in South Kalimantan, Indonesia. Over the years, our business has evolved and today, we have established a reputation as a reliable coal trader and coal shipping company in Indonesia.

						We procure thermal coal from coal mines located in South Kalimantan for domestic sales to mainly coal traders. We also provide chartering services of tugboats, barges and bulk carrier to transport coal within the Indonesian territories.

						Led by an experienced management team, and with the depth and diversity of their technical and operational expertise, we are positioned to tap opportunities in Indonesia – one of the leading producers of coal globally.

						With increases in Indonesia’s electrification and new coal-fired power plants across the Indonesian archipelago, demand for coal and inter-island transportation of coal is expected to remain robust, driving our growth.
                        </p>
                    </div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>