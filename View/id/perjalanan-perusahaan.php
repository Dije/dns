<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
  <!-- Page Preloder -->
  <div id="preloder">
    <div class="loader"></div>
  </div>

  <!-- Navbar -->
  <?php include 'include/navbar.php' ?>
  <!-- Navbar -->

  <!-- Cta Section Begin -->
  <section class="cta-section spad set-bg" data-setbg="../img/growth-journey-page-title.jpg">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cta-text">
            <h2>Perjalanan Perusahaan</h2>
            <p>Tentang Kami</p>
            <!-- <a href="#" class="primary-btn">Contact us</a> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Cta Section End -->

  <ul class="timeline"> 
    <li>
      <div class="timeline-badge"><img class="rounded-circle" src="../assets/img/Growth Journey/growth-journey-7-_resized360x260.jpg" alt="" /> </div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">2013</h4>
          <p>Anak perusahaan perdagangan batubara kami, PT DNS</p>
        </div>
        <div class="timeline-body">
          <p>Fokus pada penjualan batubara domestik
          Mendirikan anak perusahaan perdagangan batubara kami, PT DNS, untuk mengkonsolidasikan semua aktivitas perdagangan batubara di Grup dan untuk fokus pada penjualan batubara domestik.</p> 
        </div>
      </div>
    </li>
    <li class="timeline-inverted">
      <div class="timeline-badge"><img class="rounded-circle" src="../assets/img/Growth Journey/growth-journey-12-_resized360x260.jpg" alt="" /> </div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">2014</h4>
          <p>PT DNS diberikan lisensi SIUP-M</p>
        </div>
        <div class="timeline-body">
          <p>Diberikan lisensi SIUP-M
          PT DNS diberikan izin SIUP-M sebagai distributor, eksportir dan importir batubara, bijih besi, pasir besi/mangan.</p>
        </div>
      </div>
    </li>
    <li>
      <div class="timeline-badge"><img class="rounded-circle" src="../assets/img/Growth Journey/growth-journey-10-_resized360x260.jpg" alt="" /> </div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">2017</h4>
          <p>PT DNS menerima izin IUP-OPK selama tiga tahun untuk mendapatkan batubara dari empat tambang batubara di Kalimantan Selatan.</p>
        </div>
        <div class="timeline-body">
          <p>Tiga tahun lisensi IUP-OPK
          PT DNS menerima izin IUP-OPK selama tiga tahun untuk mendapatkan batubara dari empat tambang batubara di Kalimantan Selatan. Lisensi dapat diperpanjang maksimal tiga tahun untuk setiap pembaruan. Kami memulai bisnis perdagangan batubara.</p>
        </div>
      </div>
    </li>
    <li class="timeline-inverted">
      <div class="timeline-badge"><img class="rounded-circle" src="../assets/img/Growth Journey/growth-journey-11-_resized360x260.jpg" alt="" /> </div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">2019</h4>
          <p>Pencapaian besar</p>
        </div>
        <div class="timeline-body">
          <p>Pencapaian besar
            Tonggak Sejarah 1
            PT DNS mendapatkan kontrak penjualan batu bara jangka tetap dari pelanggan untuk penjualan agregat hingga 1,5 juta metrik ton (“MT”) batu bara, dan kontrak pembelian batu bara jangka tetap dari pemasok untuk pasokan agregat hingga 1,9 juta MT batu bara.

            Tonggak Sejarah 2
          PT DNS diberikan izin baru IUP-OPK lima tahun untuk pengangkutan dan penjualan batubara dari empat tambang batubara di Kalimantan Selatan. Lisensi dapat diperpanjang maksimal lima tahun untuk setiap pembaruan. </p> 
        </div>
      </div>
    </li> 
  </ul>

  <!-- Footer -->
  <?php include 'include/footer.php' ?>
  <!-- Footer -->
</body>

</html>