<?php 
class laporan
{
    private $id_laporan;
    private $judul;
    private $tahun;
    private $image;
    private $desc;
    private $pdf;
    public function __construct() {}
    // public function __construct($id_bahanbaku, $nama_bahanbaku, $stok, $keterangan)
    // {
    //     $this->id_bahanbaku = $id_bahanbaku;
    //     $this->nama_bahanbaku = $nama_bahanbaku;
    //     $this->stok = $stok;
    //     $this->keterangan = $keterangan;
    // }
    public function get_id_laporan()
    {
        return $this->id_laporan;
    }
    public function set_id_laporan($id_laporan)
    {
        $this->id_laporan = $id_laporan;
    }
    public function get_judul()
    {
        return $this->judul;
    }
    public function set_judul($judul)
    {
        $this->judul = $judul;
    }
    public function get_tahun()
    {
        return $this->tahun;
    }
    public function set_tahun($tahun)
    {
        $this->tahun = $tahun;
    }
    public function get_image()
    {
        return $this->image;
    }
    public function set_image($image)
    {
        $this->image = $image;
    }
    public function get_desc()
    {
        return $this->desc;
    }
    public function set_desc($desc)
    {
        $this->desc = $desc;
    }
    public function get_pdf()
    {
        return $this->pdf;
    }
    public function set_pdf($pdf)
    {
        $this->pdf = $pdf;
    }
}
?>