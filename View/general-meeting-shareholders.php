<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/Corporate-Governance-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>General Meeting Shareholders</h2>
						<p>Investors</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row justify-content-center">  
				<button class="button active" onClick="overviewFunction(this)"><a>GMS Announcement</a></button>
				<button class="button" onClick="auditFunction(this)"><a>GMS Invitation</a></button>
				<button class="button" onClick="remunerationFunction(this)"><a>GMS Minutes of Meeting</a></button> 
			</div>
		</div>
	</section>
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
				<div class="section-title"id="Overview"> 
					<div class="container">
				      	<div class="row">  
				      		<div class="col-sm-2">
				      			<p style="float: right;">Year:</p>
				      		</div>
				      		<div class="col-sm-10">
					        	<select onchange="searchFunction()" id="myInput" class="col-sm-4 form-control form-control-sm">
					          		<option value="" selected>All</option>
							        <option value="2021">2021</option>
							        <option value="2020">2020</option>
							        <option value="2019">2019</option>
							        <option value="2018">2018</option>
					        	</select>
					        	</br>
					        </div>
					        <table id="myTable" class="table">
					          	<tbody>
					            	<?php foreach($data as $dt) { ?>
					            	<tr>
					              		<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="img/file.png"></td>
					              		<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
					              		<td style="text-align: center;"><a class="download" href="<?php echo "admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
					              		<td hidden><?php echo $dt['Tahun']; ?></td>
				            		</tr> 
					            	<?php $no++; } ?>
					          	</tbody>
					        </table>
			        	</div>
			        </div>
				    </div>
				</div>	
			</div>
			<div class="row">
				<div class="about-text">
				<div class="section-title" id="Audit" style="display: none;">
					<div class="container">
				      	<div class="row">  
				      		<div class="col-sm-2">
				      			<p style="float: right;">Year:</p>
				      		</div>
				      		<div class="col-sm-10">
					        	<select onchange="searchFunction1()" id="myInput1" class="col-sm-4 form-control form-control-sm">
					          		<option value="" selected>All</option>
							        <option value="2021">2021</option>
							        <option value="2020">2020</option>
							        <option value="2019">2019</option>
							        <option value="2018">2018</option>
					        	</select>
					        	</br>
					        </div>
					        <table id="myTable1" class="table">
					          	<tbody>
					            	<?php foreach($data as $dt) { ?>
					            	<tr>
					              		<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="img/file.png"></td>
					              		<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
					              		<td style="text-align: center;"><a class="download" href="<?php echo "admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
					              		<td hidden><?php echo $dt['Tahun']; ?></td>
				            		</tr> 
					            	<?php $no++; } ?>
					          	</tbody>
					        </table>
			        	</div>
				    </div>
				</div>	
			</div>
			</div>
			<div class="row">
				<div class="about-text">
				<div class="section-title" id="Remuneration" style="display: none;">
					<div class="container">
				      	<div class="row">  
				      		<div class="col-sm-2">
				      			<p style="float: right;">Year:</p>
				      		</div>
				      		<div class="col-sm-10">
					        	<select onchange="searchFunction2()" id="myInput2" class="col-sm-4 form-control form-control-sm">
					          		<option value="" selected>All</option>
							        <option value="2021">2021</option>
							        <option value="2020">2020</option>
							        <option value="2019">2019</option>
							        <option value="2018">2018</option>
					        	</select>
					       		</br>
					        </div>
					        <table id="myTable2" class="table">
					          	<tbody>
					            	<?php foreach($data as $dt) { ?>
					            	<tr>
					              		<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="img/file.png"></td>
					              		<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
					              		<td style="text-align: center;"><a class="download" href="<?php echo "admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
					              		<td hidden><?php echo $dt['Tahun']; ?></td>
				            		</tr> 
					            	<?php $no++; } ?>
					          	</tbody>
					        </table>
			        	</div>
				    </div>
				</div>	
			</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>