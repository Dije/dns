<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->
    <?php include 'include/navbar.php' ?>
    <!-- Navbar -->

    <!-- Cta Section Begin -->
    <section class="cta-section spad set-bg" data-setbg="img/bod-page-title.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-text">
                        <h2>Board Of Commissioners</h2>
                        <p>About Us</p>
                        <!-- <a href="#" class="primary-btn">Contact us</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cta Section End -->

    <!-- Testimoial Section Begin -->
    <section class="testimonial-section">
        <div class="container">
            <div class="fs-about"> 
                <div class="fa-logo1">
                    <div class="content row">  
                        <div class="col-sm-3">
                            <img class="rounded-circle" src="img/img_avatar2.png" alt="" >
                        </div>
                        <div class="col-sm-8">
                            <div class="fa-name"> 
                                <h2>CEO</h2>
                                <p>CEO</p>
                            </div>  
                        </div> 
                        <div class="col-sm-1">
                            <h2 class="expand" onClick="myFunction()" id="click_advance"><i class="fa fa-angle-double-down"></i></h2>
                        </div> 
                    </div> 
                </div> 
                <div class="container">  
                    <div class="about-text">
                        <div class="section-title" id="myDIV" style="display: none;"> 
                            <p>Mr Salim Limanto is responsible for the overall operations and business development activities of our Group. Mr Limanto has over 11 years of management and business development experience in the coal mining, transportation and trading industries, and has been involved in the Group’s business since the inception of PT DNS and PT DPAL in 2013 and 2010, respectively.

                                Mr Limanto started his career in PT Sinar Deli, which was previously one of the domestic coal trading entities of the Deli Coal Group, where he was Head of Sales and Shipping from 2006 to 2018. He holds directorships in several companies such as PT DNS and PT DPAL.

                            Mr Limanto obtained a Bachelor Degree from the Universitas Tarumanagara, Jakarta, Indonesia in 2006. He is the eldest son of Mr Djunaidi Hardi and a nephew of Mr Juhadi, Mr Arifin Ang and Mr Limas Ananto, all of whom are Founding Shareholders of our Company.</p>
                        </div>  
                    </div>
                </div>
                <div class="fa-logo1">
                    <div class="content row">  
                        <div class="col-sm-3">
                            <img class="rounded-circle" src="img/img_avatar2.png" alt="" >
                        </div>
                        <div class="col-sm-8">
                            <div class="fa-name"> 
                                <h2>COO</h2>
                                <p>COO</p> 
                            </div>  
                        </div> 
                        <div class="col-sm-1">
                            <h2 class="expand" onClick="myFunction2()" id="click_advance2"><i class="fa fa-angle-double-down"></i></h2>
                        </div> 
                    </div> 
                </div>          
                <div class="container">  
                    <div class="about-text">
                        <div class="section-title" id="myDIV2" style="display: none;"> 
                            <p>Mr Yeo Tze Khern is responsible for the accounting and financial functions of the Group.

                                Mr Yeo started his career as an Auditor in Ernst & Young (Singapore) from 1999 to 2002. From 2002 to 2005, he was an Audit Manager in Ernst & Young Hua Ming (Beijing, China). Subsequently Mr Yeo joined Lehmanbrown International Accounting (Shanghai, China) as a Senior Manager from 2005 to 2007. From 2007 to 2009, he was a Director at PKF International Accounting (Shanghai, China). From 2009 to 2018, Mr Yeo acted as the Chief Financial Officer and Company Secretary of China Mining International Limited, a company listed on the Main Board of SGX-ST. In 2018, Mr Yeo joined RID as the Chief Financial officer before he was subsequently transferred to the Company.

                            Mr Yeo graduated with a Bachelor of Business (Marketing) from Monash University, Australia in 1997 and obtained a Master of Practising Accounting from Monash University, Australia in 1999. He is a Chartered Accountant and a member of the Institute of Singapore Chartered Accountants, and fellows of CPA Australia and the Hong Kong Institute of Certified Public Accountants. Mr Yeo is also a member of the Singapore Institute of Directors.</p>
                        </div>
                    </div>
                </div>
                <div class="fa-logo1">
                    <div class="content row">  
                        <div class="col-sm-3">
                            <img class="rounded-circle" src="img/img_avatar2.png" alt="" >
                        </div>
                        <div class="col-sm-8">
                            <div class="fa-name"> 
                                <h2>CFO</h2>
                                <p>CFO</p> 
                            </div>  
                        </div> 
                        <div class="col-sm-1">
                            <h2 class="expand" onClick="myFunction3()" id="click_advance3"><i class="fa fa-angle-double-down"></i></h2>
                        </div> 
                    </div> 
                </div>          
                <div class="container">  
                    <div class="about-text">
                        <div class="section-title" id="myDIV3" style="display: none;"> 
                            <p>Mr Yeo Tze Khern is responsible for the accounting and financial functions of the Group.

                                Mr Yeo started his career as an Auditor in Ernst & Young (Singapore) from 1999 to 2002. From 2002 to 2005, he was an Audit Manager in Ernst & Young Hua Ming (Beijing, China). Subsequently Mr Yeo joined Lehmanbrown International Accounting (Shanghai, China) as a Senior Manager from 2005 to 2007. From 2007 to 2009, he was a Director at PKF International Accounting (Shanghai, China). From 2009 to 2018, Mr Yeo acted as the Chief Financial Officer and Company Secretary of China Mining International Limited, a company listed on the Main Board of SGX-ST. In 2018, Mr Yeo joined RID as the Chief Financial officer before he was subsequently transferred to the Company.

                            Mr Yeo graduated with a Bachelor of Business (Marketing) from Monash University, Australia in 1997 and obtained a Master of Practising Accounting from Monash University, Australia in 1999. He is a Chartered Accountant and a member of the Institute of Singapore Chartered Accountants, and fellows of CPA Australia and the Hong Kong Institute of Certified Public Accountants. Mr Yeo is also a member of the Singapore Institute of Directors.</p>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </section>
    <!-- Testimonial Section End -->
    
    <div class="container"> 
    </div>
    <!-- Footer -->
    <?php include 'include/footer.php' ?>
    <!-- Footer -->
</body>

</html>

