<?php
class financialHighlightController{
	protected $conn;

	public function __construct(){
		$this->conn = mysqli_connect("192.168.100.88", "deli", "Deli123", "website", "3306"); //(host, username, password, database, port)
	}

	public function getData(){
		$query = mysqli_query($this->conn,"SELECT * FROM FinancialHighlight WHERE delete_date IS NULL");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	}

	public function getDataByUid($uID){
		$query = mysqli_query($this->conn,"SELECT * FROM FinancialHighlight WHERE ID_Laporan='$uID'");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	}
	
	public function addReport($Judul, $Tahun, $Desc, $pdf, $createddate){
		$query="SELECT * FROM FinancialHighlight WHERE PDF = '$pdf'";
		$result = mysqli_query($this->conn,$query);
		$count_row = $result->num_rows;
		if ($count_row == 0){
			$query="INSERT INTO FinancialHighlight(Judul, Tahun, Des, PDF, created_date) VALUES ('$Judul', '$Tahun', '$Desc', '$pdf', '$createddate')";
			$result = mysqli_query($this->conn,$query) or die(mysqli_connect_errno()."Data cannot inserted");
			return $result;
		}
		else{
			return false;
		}
	}

	public function updateDataByUID($Judul, $Tahun, $Desc, $pdf, $updatedate, $uID){
		$result = mysqli_query($this->conn,$query);
		$count_row = $result->num_rows;
		if ($count_row == 0){
			$query = "UPDATE FinancialHighlight SET Judul = '$Judul', Tahun = '$Tahun', Des = '$Desc', PDF = '$pdf', update_date = '$updatedate' WHERE ID_Laporan = '$uID'";
			$result = mysqli_query($this->conn,$query) or die(mysqli_connect_errno()."Data cannot inserted");
			return $result;
		}
		else{
			return false;
		}
	}

	public function updateDataWithoutFileByUID($Judul, $Tahun, $Desc, $updatedate, $uID){
		$query = "UPDATE FinancialHighlight SET Judul = '$Judul', Tahun = '$Tahun', Des = '$Desc', update_date = '$updatedate' WHERE ID_Laporan = '$uID'";
		$result = mysqli_query($this->conn,$query) or die(mysqli_connect_errno()."Data cannot inserted");
		return $result;
	}

	public function deleteReport($deletedate, $IDReport){
		$query = "SELECT * FROM FinancialHighlight WHERE ID_Laporan='$IDReport'";
            //checking if the data is available in db
		$result = mysqli_query($this->conn,$query);
		$count_row = $result->num_rows;
		if ($count_row == 1){
			$query = "UPDATE FinancialHighlight SET delete_date = '$deletedate' WHERE ID_Laporan='$IDReport'";
			$result = mysqli_query($this->conn,$query) or die(mysqli_connect_errno()."Data cannot inserted");
			return $result; 
		}
		else { 
			return false;
		}
	}
}
?>

