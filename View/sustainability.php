<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/Sustainability-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Sustainability</h2>
						<p><br /></p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section set-bg" data-setbg="img/Subbar-Business-Overview.png">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title quotes">
						<h4>We strive to achieve growth in a sustainable manner and be a responsible corporate citizen towards the wider community and the environment.</h4>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"> 
						<p>As a Group, we recognise the importance of placing sustainability at the heart of our business strategy. In all that we do, we have responsibilities towards our society, staff, shareholders, and the environment.

							Equally important is our commitment in ensuring a safe and healthy work environment for our staff – a key asset in driving success of our business. Our investment in training ensures that our employees are equipped with the necessary skills and knowledge, and relevant certifications to perform their duties.

							In the area of environmental protection, while our operational activities do not generate industrial pollutants or hazardous and toxic waste, our vessel crews fulfil the required construction and equipment related regulations preventing pollution, as well as compliance with the relevant anti-dumping regulations in Indonesia.
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>