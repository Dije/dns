<!-- Js Plugins -->
<script src="../js/jquery-3.3.1.min.js"></script> 
<script src="../js/jquery.magnific-popup.min.js"></script>
<script src="../js/jquery.easing.min.js"></script>
<script src="../js/isotope.pkgd.min.js"></script>
<script src="../js/masonry.pkgd.min.js"></script>
<script src="../js/jquery.slicknav.js"></script> 
<script src="../js/main.js"></script>
<script src="../js/footer.js"></script> 
<script src="https://www.google.com/recaptcha/api.js"></script>
<script src="../assets/recaptcha/validator.js"></script> 
<script src="../assets/recaptcha/contact.js"></script> 
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


<script>
(function() {
    var cx = 'caf30d28032f80ffc';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
})();
	window.onload = function(){
	document.getElementById('gsc-i-id1').placeholder = 'Search...';
	document.getElementById('gsc-i-id2').placeholder = 'Search...';
}
</script>

<script>
	$(function() {
	  AOS.init();
	});
</script>

<script >
	$('#click_advance').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script >
	$('#click_advance2').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	$('#click_advance3').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	function myFunction() { 
			var x = document.getElementById("myDIV");
			var y = document.getElementById("myDIV2");
			var z = document.getElementById("myDIV3");
			if (x.style.display === "none") {
				x.style.display = "block";
				y.style.display = "none";
				z.style.display = "none";
			} else {
				x.style.display = "none";
			}
		} 
	function myFunction2() {  
		var x = document.getElementById("myDIV");
		var y = document.getElementById("myDIV2");
		var z = document.getElementById("myDIV3");
		if (y.style.display === "none") {
			x.style.display = "none";
			y.style.display = "block";
			z.style.display = "none";
		} else {
			y.style.display = "none";
		}
	} 
	function myFunction3() {
		var x = document.getElementById("myDIV");
		var y = document.getElementById("myDIV2");
		var z = document.getElementById("myDIV3");
		if (z.style.display === "none") {
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "block";
		} else {
			z.style.display = "none";
		}
	} 
	function myFunction4() {  
		var a = document.getElementById("myDIV4");
		var b = document.getElementById("myDIV5");
		if (a.style.display === "none") { 
			a.style.display = "block";
			b.style.display = "none";
		} else {
			a.style.display = "none";
		}
	} 
	function myFunction5() {
		var a = document.getElementById("myDIV4");
		var b = document.getElementById("myDIV5");
		if (b.style.display === "none") {
			a.style.display = "none";
			b.style.display = "block";
		} else {
			b.style.display = "none";
		}
	} 
</script>

<script>
	function overviewFunction(elem) {
		var a = document.getElementById("Overview");
		var b = document.getElementById("Audit");
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating");
		var e = document.getElementById("Board");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "block";  
			b.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "none";  
			e.style.display = "none";  
		} else {
			// a.style.display = "none";
		}
	}
	function auditFunction(elem) {
		var a = document.getElementById("Overview");
		var b = document.getElementById("Audit");
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating");
		var e = document.getElementById("Board");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none";  
			b.style.display = "block"; 
			c.style.display = "none"; 
			d.style.display = "none"; 
			e.style.display = "none"; 
		} else {
			// b.style.display = "none";
		}
	}
	function remunerationFunction(elem) {
		var a = document.getElementById("Overview");
		var b = document.getElementById("Audit");
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating");
		var e = document.getElementById("Board");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (c.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "block"; 
			d.style.display = "none"; 
			e.style.display = "none"; 
		} else {
			// c.style.display = "none";
		}
	}
	function nominatingFunction(elem) {
		var a = document.getElementById("Overview");
		var b = document.getElementById("Audit");
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating");
		var e = document.getElementById("Board");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (d.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "block"; 
			e.style.display = "none"; 
		} else {
			// d.style.display = "none";
		}
	}
	function boardFunction(elem) {
		var a = document.getElementById("Overview");
		var b = document.getElementById("Audit");
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating");
		var e = document.getElementById("Board");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (e.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "none"; 
			e.style.display = "block"; 
		} else {
			// e.style.display = "none";
		}
	}
</script>

<script>
	function overviewBusinessFunction(elem) {
		var a = document.getElementById("OverviewBusiness");
		var b = document.getElementById("TradingRiskmanagement"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "block"; 
			b.style.display = "none";  
		} else {
			// a.style.display = "none";
		}
	}
	function TradingRiskManagementFunction(elem) {
		var a = document.getElementById("OverviewBusiness");
		var b = document.getElementById("TradingRiskmanagement"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "block";  
		} else {
			// b.style.display = "none";
		}
	}
</script>

<script>
	function overviewShippingFunction(elem) {
		var a = document.getElementById("OverviewShipping");
		var b = document.getElementById("Videos"); 
		var c = document.getElementById("Fleet");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "block"; 
			b.style.display = "none";  
			c.style.display = "none"; 
		} else {
			// a.style.display = "none";
		}
	}
	function videosFunction(elem) {
		var a = document.getElementById("OverviewShipping");
		var b = document.getElementById("Videos"); 
		var c = document.getElementById("Fleet");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "block";  
			c.style.display = "none"; 
		} else {
			// b.style.display = "none";
		}
	}
	function fleetFunction(elem) {
		var a = document.getElementById("OverviewShipping");
		var b = document.getElementById("Videos");
		var c = document.getElementById("Fleet");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active'); 
		if (c.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "block";  
		} else {
			// c.style.display = "none";
		}
	}
</script>

<script>
	$('#myModal').on('shown.bs.modal', function () {
		$('#video')[0].play();
	});
	$('#myModal2').on('shown.bs.modal', function () {
		$('#video2')[0].play();
	});
	$('#myModal3').on('shown.bs.modal', function () {
		$('#video3')[0].play();
	});
	$(window).on('hidden.bs.modal', function () {
		$('#video')[0].pause();
		$('#video2')[0].pause();
		$('#video3')[0].pause();
	})
</script>


<script>
	function searchFunction() {
		var input, filter, table, tr, td, i, txtValue;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		table = document.getElementById("myTable");
		tr = table.getElementsByTagName("tr");
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[3];
			if (td) {
				txtValue = td.textContent || td.innerText;
				if (txtValue.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			} 
		}
	}
</script>

<script>
function searchFunction1() {
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("myInput1");
	filter = input.value.toUpperCase();
	table = document.getElementById("myTable1");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[3];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		} 
	}
}
</script>

<script>
function searchFunction2() {
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("myInput2");
	filter = input.value.toUpperCase();
	table = document.getElementById("myTable2");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[3];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		} 
	}
}
</script>

<script>
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({ 
		content: '<a class="close1" data-dismiss="alert" href="#/">Salim Limanto</a>',
		html: true
	}); 
	$(document).on("click", ".popover .close1" , function(){
		$(this).parents(".popover").popover('hide');
		$('#myModal4').modal('show');
	});
});
</script>  

<script>
$('.normal').owlCarousel({
    loop:true, 
    nav:true,
    autoplay:true,
    autoplayTimeout:9000,
    responsive:{
        0:{
            items:1
        },
        640:{
            items:1
        },
      	960:{
            items:1
        },
        1200:{
            items:1
        }
    } 
})
</script> 

<script>
/*Scroll to top when arrow up clicked BEGIN*/
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 100) {
        $('#back2Top').fadeIn();
    } else {
        $('#back2Top').fadeOut();
    }
});
$(document).ready(function() {
    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});
 /*Scroll to top when arrow up clicked END*/
 </script>  

<script>
$(function () { 
    if (window.location.hash === "#TradingRiskManagement") { 
        var a = document.getElementById("OverviewBusiness");
		var b = document.getElementById("TradingRiskmanagement"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		btn[1].classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "block";  
		} else {
			// b.style.display = "none";
		}
    } 
}); 
</script> 	


<script>
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'AUDIT'
    }, 
    xAxis: { 
        categories: ['Revenue', 'Profit before tax', 'Profit attributable to equity holders of the Company', 'Earnings per share (1) (cents)', 'Non-current assets', 'Current assets', 'Non-current liabilities', 'Current liabilities', '(NAV) attributable to equity holders of the Company', 'NAV per share (1) (cents)'],
        title: {
            text: null
        }
    },
    yAxis: {
    	min: 0,
        title: {
            text: 'Financial Year End 31 Dec (SGD’000)'
        },
        labels: {
            overflow: 'justify'
        }
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    series: [{
        name: 'FY2016',
        data: <?php echo json_encode($FY2016, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'FY2017',
        data: <?php echo json_encode($FY2017, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'FY2018',
        data: <?php echo json_encode($FY2018, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'FY2019',
        data: <?php echo json_encode($FY2019, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'FY2020',
        data: <?php echo json_encode($FY2020, JSON_NUMERIC_CHECK); ?>
    }]
});
</script>  