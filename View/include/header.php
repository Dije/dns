<?php
define('PROJECT_ROOT_PATH', __DIR__);
session_start();
include_once (PROJECT_ROOT_PATH . '/../../Controller/laporanController.php');
include_once (PROJECT_ROOT_PATH . '/../../Controller/financialStatementController.php'); 
include_once (PROJECT_ROOT_PATH . '/../../Controller/financialHighlightController.php');  
include_once (PROJECT_ROOT_PATH . '/../../Controller/auditController.php');  
$financialStatement = new financialStatementController(); 
$financialHighlight = new financialHighlightController(); 
$audit = new auditController(); 
$laporan = new laporanController();

$dataFS = $financialStatement->getData(); 
$dataFH = $financialHighlight->getData(); 
$data = $laporan->getData();
$no = 1;
?> 
<!DOCTYPE html>
<html lang="zxx">

<head> 
    <meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DNS</title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Quantico:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet"> 
    <!-- Css Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.20/dist/css/uikit.min.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">  
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css"> 
    <link rel="stylesheet" href="assets/icofont/icofont.min.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
   
</head>