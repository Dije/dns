<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/management-team-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Dewan Direksi</h2>
						<p>Tentang Kami</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="fs-about"> 
				<div class="fa-logo1"> 
					<div class="content row">  
						<div class="col-sm-3">
							<img class="rounded-circle" src="../assets/img/Board of Directors/Salim-Limanto-1.jpg" alt="" >
						</div>
						<div class="col-sm-8">
							<div class="fa-name"> 
								<h2>Salim Limanto</h2>
								<p>DIREKTUR EKSEKUTIF DAN KEPALA OPERASIONAL OFFICER</p>
							</div>  
						</div> 
						<div class="col-sm-1">
							<h2 class="expand" onClick="myFunction4()" id="click_advance2"><i class="fa fa-angle-double-down"></i></h2>
						</div> 
					</div> 
				</div> 
				<div class="container">  
					<div class="about-text">
						<div class="section-title" id="myDIV4" style="display: none;"> 
							<p>Bapak Salim Limanto bertanggung jawab atas keseluruhan operasi dan kegiatan pengembangan bisnis Grup kita. Bapak Limanto memiliki lebih dari 11 tahun pengalaman manajemen dan pengembangan bisnis di pertambangan batubara, transportasi dan industri perdagangan, dan telah terlibat dalam bisnis Grup sejak dimulainya PT DNS dan PT DPAL masing-masing pada tahun 2013 dan 2010.

							Bapak Limanto memulai karirnya di PT Sinar Deli, yang sebelumnya merupakan salah satu entitas perdagangan batubara domestik dari Grup Deli Coal, dimana beliau menjabat sebagai Kepala dari Penjualan dan Pengiriman dari tahun 2006 hingga 2018. Beliau menjabat sebagai direktur di beberapa perusahaan seperti PT DNS dan PT DPAL.

							Bapak Limanto memperoleh gelar Sarjana dari Universitas Tarumanagara, Jakarta, Indonesia pada tahun 2006. Beliau adalah putra tertua dari Bapak Djunaidi Hardi dan keponakan dari Bapak Juhadi, Bapak Arifin Ang dan Bapak Limas Ananto, yang semuanya merupakan Pemegang Saham Pendiri Perusahaan kami.</p>
						</div>  
					</div>
				</div>
				<div class="fa-logo1">
					<div class="content row">  
						<div class="col-sm-3">
							<img class="rounded-circle" src="../assets/img/Board of Directors/Thomas-Yeo-Tze-Khern.jpg" alt="" >
						</div>
						<div class="col-sm-8">
							<div class="fa-name"> 
								<h2>Yeo Tze Khern </h2>
								<p>KEPALA BAGIAN KEUANGAN</p> 
							</div>  
						</div> 
						<div class="col-sm-1">
							<h2 class="expand" onClick="myFunction5()" id="click_advance3"><i class="fa fa-angle-double-down"></i></h2>
						</div> 
					</div> 
				</div>  
				<div class="container">  
					<div class="about-text">
						<div class="section-title" id="myDIV5" style="display: none;"> 
							<p>Bapak Yeo Tze Khern bertanggung jawab atas fungsi akuntansi dan keuangan Grup.

							Bapak Yeo memulai karirnya sebagai Auditor di Ernst & Young (Singapura) dari 1999 sampai 2002. Dari 2002 sampai 2005, dia adalah Manajer Audit di Ernst & Young Hua Ming (Beijing, Cina). Selanjutnya Bapak Yeo bergabung dengan Lehmanbrown International Accounting (Shanghai, China) sebagai Manajer Senior dari tahun 2005 sampai 2007. Dari tahun 2007 sampai 2009, beliau menjabat sebagai Direktur di PKF International Accounting (Shanghai, Cina). Dari 2009 hingga 2018, Bapak Yeo bertindak sebagai Kepala Bagian Keuangan dan Sekretaris Perusahaan China Mining International Limited, perusahaan yang terdaftar di Papan Utama SGX-ST. Pada tahun 2018, Bapak Yeo bergabung dengan RID sebagai Kepala Bagian Keuangan sebelum kemudian dipindahkan ke Perusahaan.

							Bapak Yeo lulus dengan gelar Bachelor of Business (Marketing) dari Monash University, Australia pada tahun 1997 dan memperoleh gelar Master of Practicing Accounting dari Monash University, Australia pada tahun 1999. Ia adalah Chartered Accountant dan anggota dari Institute of Singapore Chartered Accountants, dan rekan dari CPA Australia dan Institut Akuntan Publik Hong Kong. Mr Yeo juga merupakan anggota dari Singapore Institute of Directors.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>