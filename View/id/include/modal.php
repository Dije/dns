<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <video id="video" width=100% controls>
          <source src="../assets/img/Coal Shipping/Videos/Videos - RGD.mp4" type="video/mp4"> 
          </video>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <video id="video2" width=100% controls>
            <source src="../assets/img/Coal Shipping/Videos/Videos - RGD_2.mp4" type="video/mp4"> 
            </video>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModal3" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <video id="video3" width=100% controls>
              <source src="../assets/img/Coal Shipping/Videos/Videos - RGD_3.mp4" type="video/mp4"> 
              </video>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="myModal4" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="row">
                <div class="about-text margin">
                  <div class="section-title">
                    <div class="container">
                      <div class="row">  
                        <div class="col-sm-4">
                          <img class="image" src="../assets/img/Board of Directors/Salim-Limanto-1.jpg"> 
                        </div>
                        <div class="col-sm-8">
                          <h5>EXECUTIVE DIRECTOR AND CHIEF OPERATING OFFICER</h5>
                          <h2>Salim Limanto</h2>
                          <p>Mr Salim Limanto is responsible for the overall operations and business development activities of our Group. Mr Limanto has over 11 years of management and business development experience in the coal mining, transportation and trading industries, and has been involved in the Group’s business since the inception of PT DNS and PT DPAL in 2013 and 2010, respectively.

                          Mr Limanto started his career in PT Sinar Deli, which was previously one of the domestic coal trading entities of the Deli Coal Group, where he was Head of Sales and Shipping from 2006 to 2018. He holds directorships in several companies such as PT DNS and PT DPAL.

                          Mr Limanto obtained a Bachelor Degree from the Universitas Tarumanagara, Jakarta, Indonesia in 2006. He is the eldest son of Mr Djunaidi Hardi and a nephew of Mr Juhadi, Mr Arifin Ang and Mr Limas Ananto, all of whom are Founding Shareholders of our Company.</p>
                        </div>
                      </div>
                    </div>  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>