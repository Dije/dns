<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->
	
	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/cta-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Sekilas Perusahaan</h2>
						<p>Tentang Kami</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section set-bg" data-setbg="../img/Subbar-Business-Overview.png">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title quotes">
						<h4>Dengan rekam jejak kualitas layanan yang konsisten dan pengiriman tepat waktu, ditambah dengan hubungan jangka panjang kami dengan pelanggan, kami telah membangun reputasi sebagai pedagang batubara dan perusahaan pengiriman batubara yang andal di Indonesia.</h4>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"> 
                        <p>Sejarah keterlibatan kami dalam industri batubara dapat ditelusuri kembali ke sekitar tahun 2005 di Kalimantan Selatan, Indonesia. Selama bertahun-tahun, bisnis kami telah berkembang dan hari ini, kami telah membangun reputasi sebagai pedagang batubara dan perusahaan pengiriman batubara yang andal di Indonesia.

						Kami membeli batubara termal dari tambang batubara yang berlokasi di Kalimantan Selatan untuk penjualan domestik sebagian besar ke pedagang batubara. Kami juga menyediakan layanan penyewaan kapal tunda, tongkang dan kapal curah untuk mengangkut batubara di wilayah Indonesia.

						Dipimpin oleh tim manajemen yang berpengalaman, dan dengan kedalaman serta keragaman keahlian teknis dan operasional mereka, kami siap untuk memanfaatkan peluang di Indonesia - salah satu produsen batubara terkemuka di dunia.

						Dengan meningkatnya elektrifikasi Indonesia dan pembangkit listrik tenaga batu bara baru di seluruh kepulauan Indonesia, permintaan batu bara dan transportasi antar pulau untuk batu bara diharapkan tetap kuat, mendorong pertumbuhan kami.
                        </p>
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>