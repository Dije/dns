<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/csr-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Corporate Social Responsibility</h2>
						<p>Sustainability</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12"> 
					<div class="about-text">
					<div class="section-title"> 
						<p> The Group supports the local communities around us. From time to time, the local communities also utilise our premises in North Jakarta to hold community events and social gatherings.

							Looking ahead, our corporate and social responsibility (CSR) initiatives will continue to evolve as we manage our business and operating activities.

						To augment our CSR initiatives, we will continue to assess and manage the impact our operations have on the environment and our stakeholders, including the local community, our customers, employees, suppliers and shareholders.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>