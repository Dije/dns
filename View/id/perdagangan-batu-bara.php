<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/trading-risk-management-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Perdagangan Batu Bara</h2>
						<p>Bisnis Kami</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row justify-content-center">            
				<button class="button active" onClick="overviewBusinessFunction(this)"><a>Gambaran Umum</a></button>
				<button class="button" onClick="TradingRiskManagementFunction(this)"><a>Manajemen Risiko Perdagangan</a></button>
			</div>
		</div>
	</section>
	<div id="OverviewBusiness" >
		<section class="testimonial-section set-bg" data-setbg="../img/Subbar-Business-Overview.png">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section-title quotes">
							<h4>Hubungan kita dengan pemasok memungkinkan akses ke sumber batubara yang andal dengan harga bersaing.</h4>
						</div>
					</div>
				</div>
			</section>
			<section class="testimonial-section">
				<div class="container">
					<div class="row">
						<div class="about-text">
							<div class="section-title"> 
								<p>Kami menjalankan Bisnis Perdagangan Batubara melalui anak perusahaan kami, PT DNS. Batubara termal yang kami hasilkan dipasok terutama ke pedagang batubara yang melayani pengguna akhir domestik di berbagai industri, termasuk peleburan nikel dan manufaktur semen. 

									Salah satu kekuatan utama kami adalah pasokan batubara yang andal. Kami telah dan akan terus melakukan pengadaan batu bara dari tambang batu bara pihak ketiga dan tambang batu bara terkait dengan Pemegang Saham Pendiri kami.

									Pada tahun buku 2019, kami mendapatkan kontrak penjualan batu bara jangka tetap dari pelanggan untuk penjualan agregat hingga 1,5 juta metrik ton <b> (“MT”) </b> batu bara, dan kontrak pembelian batu bara jangka tetap dari pemasok untuk agregat pasokan batubara hingga 1,9 juta MT.

								Grup dapat mengadakan kontrak penjualan dan/atau pembelian batu bara berjangka tetap dengan pelanggan dan pemasok kami, tergantung pada penilaian kami terhadap kondisi pasar dan peluang. </p>
							</div>
						</div>
					</div>  
				</div>
			</div>
		</section>
	</div> 
	<section class="testimonial-section" id="TradingRiskmanagement" style="display: none;">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="timeline-image"><img class="img-fluid" 
							src="../assets/img/Trading Risk Management/trading-risk-management.jpg" alt="" />
						</div>
					</div>
					<div class="col-md-6 about-text">
						<div class="section-title">
						<p class="detail">Untuk mengelola risiko perdagangan, kami biasanya mencari batu bara setelah mendapatkan kontrak pembelian batu bara dengan pelanggan kami (yaitu kontrak penjualan dan pembelian batu bara dengan pemasok dan pelanggan kami masing-masing dibuat secara back-to-back). Oleh karena itu, kami tidak mempertahankan stok batubara atau mengambil posisi signifikan pada harga batubara.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>