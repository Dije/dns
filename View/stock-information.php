<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/cta-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Stock Information</h2>
						<p>Investors</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="about-text"> 
					<div class="section-title">
						<p style="text-align: center;">As of December 31, 2018, PT Deli Niaga Sejahtera (DNS) had a total of 3,333,333,500 shares.</p>
					</div>
				</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Testimoial Section Begin -->
	<!-- <section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"> 
                        <p>Our history of being involved in the coal industry can be traced back to around 2005 in South Kalimantan, Indonesia. Over the years, our business has evolved and today, we have established a reputation as a reliable coal trader and coal shipping company in Indonesia.

						We procure thermal coal from coal mines located in South Kalimantan for domestic sales to mainly coal traders. We also provide chartering services of tugboats, barges and bulk carrier to transport coal within the Indonesian territories.

						Led by an experienced management team, and with the depth and diversity of their technical and operational expertise, we are positioned to tap opportunities in Indonesia – one of the leading producers of coal globally.

						With increases in Indonesia’s electrification and new coal-fired power plants across the Indonesian archipelago, demand for coal and inter-island transportation of coal is expected to remain robust, driving our growth.
                        </p>
                    </div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>