<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->


<?php 
$dataAu = $audit->getDataFY2016(); 
$data17 = $audit->getDataFY2017(); 
$data18 = $audit->getDataFY2018(); 
$data19 = $audit->getDataFY2019(); 
$data20 = $audit->getDataFY2020();  
$Rev = array();  
$dt17 = array();
$dt18 = array();
$dt19 = array();
$dt20 = array(); 
foreach ($dataAu as $dr ) { 
	$revenue = (array(array("y"=>$dr['Revenue']))); 
	$ProfBfTax = (array(array("y"=>$dr['ProfitBfTax']))); 
	$profatt = (array(array("y"=>$dr['ProfitAttributable']))); 
	$earnps = (array(array("y"=>$dr['EarningsPerShare']))); 
	$ncurrasset = (array(array("y"=>$dr['NonCurrentAsset']))); 
	$currasset = (array(array("y"=>$dr['CurrentAssets']))); 
	$ncurrlia = (array(array("y"=>$dr['NonCurrentLia']))); 
	$currlia = (array(array("y"=>$dr['CurrentLia']))); 
	$attequi = (array(array("y"=>$dr['AttEquity']))); 
	$navpersh = (array(array("y"=>$dr['NavPerShare']))); 
	$Rev = array_merge($Rev,$revenue); 
	$Rev = array_merge($Rev,$ProfBfTax); 
	$Rev = array_merge($Rev,$profatt); 
	$Rev = array_merge($Rev,$earnps); 
	$Rev = array_merge($Rev,$ncurrasset); 
	$Rev = array_merge($Rev,$currasset);  
	$Rev = array_merge($Rev,$ncurrlia);  
	$Rev = array_merge($Rev,$currlia);  
	$Rev = array_merge($Rev,$attequi);  
	$Rev = array_merge($Rev,$navpersh);  
}   

foreach ($data17 as $dr ) { 
	$revenue = (array(array("y"=>$dr['Revenue']))); 
	$ProfBfTax = (array(array("y"=>$dr['ProfitBfTax']))); 
	$profatt = (array(array("y"=>$dr['ProfitAttributable']))); 
	$earnps = (array(array("y"=>$dr['EarningsPerShare']))); 
	$ncurrasset = (array(array("y"=>$dr['NonCurrentAsset']))); 
	$currasset = (array(array("y"=>$dr['CurrentAssets']))); 
	$ncurrlia = (array(array("y"=>$dr['NonCurrentLia']))); 
	$currlia = (array(array("y"=>$dr['CurrentLia']))); 
	$attequi = (array(array("y"=>$dr['AttEquity']))); 
	$navpersh = (array(array("y"=>$dr['NavPerShare']))); 
	$dt17 = array_merge($dt17,$revenue); 
	$dt17 = array_merge($dt17,$ProfBfTax); 
	$dt17 = array_merge($dt17,$profatt); 
	$dt17 = array_merge($dt17,$earnps); 
	$dt17 = array_merge($dt17,$ncurrasset); 
	$dt17 = array_merge($dt17,$currasset);  
	$dt17 = array_merge($dt17,$ncurrlia);  
	$dt17 = array_merge($dt17,$currlia);  
	$dt17 = array_merge($dt17,$attequi);  
	$dt17 = array_merge($dt17,$navpersh);  
}   

foreach ($data18 as $dr ) { 
	$revenue = (array(array("y"=>$dr['Revenue']))); 
	$ProfBfTax = (array(array("y"=>$dr['ProfitBfTax']))); 
	$profatt = (array(array("y"=>$dr['ProfitAttributable']))); 
	$earnps = (array(array("y"=>$dr['EarningsPerShare']))); 
	$ncurrasset = (array(array("y"=>$dr['NonCurrentAsset']))); 
	$currasset = (array(array("y"=>$dr['CurrentAssets']))); 
	$ncurrlia = (array(array("y"=>$dr['NonCurrentLia']))); 
	$currlia = (array(array("y"=>$dr['CurrentLia']))); 
	$attequi = (array(array("y"=>$dr['AttEquity']))); 
	$navpersh = (array(array("y"=>$dr['NavPerShare']))); 
	$dt18 = array_merge($dt18,$revenue); 
	$dt18 = array_merge($dt18,$ProfBfTax); 
	$dt18 = array_merge($dt18,$profatt); 
	$dt18 = array_merge($dt18,$earnps); 
	$dt18 = array_merge($dt18,$ncurrasset); 
	$dt18 = array_merge($dt18,$currasset);  
	$dt18 = array_merge($dt18,$ncurrlia);  
	$dt18 = array_merge($dt18,$currlia);  
	$dt18 = array_merge($dt18,$attequi);  
	$dt18 = array_merge($dt18,$navpersh);  
}   

foreach ($data19 as $dr ) { 
	$revenue = (array(array("y"=>$dr['Revenue']))); 
	$ProfBfTax = (array(array("y"=>$dr['ProfitBfTax']))); 
	$profatt = (array(array("y"=>$dr['ProfitAttributable']))); 
	$earnps = (array(array("y"=>$dr['EarningsPerShare']))); 
	$ncurrasset = (array(array("y"=>$dr['NonCurrentAsset']))); 
	$currasset = (array(array("y"=>$dr['CurrentAssets']))); 
	$ncurrlia = (array(array("y"=>$dr['NonCurrentLia']))); 
	$currlia = (array(array("y"=>$dr['CurrentLia']))); 
	$attequi = (array(array("y"=>$dr['AttEquity']))); 
	$navpersh = (array(array("y"=>$dr['NavPerShare']))); 
	$dt19 = array_merge($dt19,$revenue); 
	$dt19 = array_merge($dt19,$ProfBfTax); 
	$dt19 = array_merge($dt19,$profatt); 
	$dt19 = array_merge($dt19,$earnps); 
	$dt19 = array_merge($dt19,$ncurrasset); 
	$dt19 = array_merge($dt19,$currasset);  
	$dt19 = array_merge($dt19,$ncurrlia);  
	$dt19 = array_merge($dt19,$currlia);  
	$dt19 = array_merge($dt19,$attequi);  
	$dt19 = array_merge($dt19,$navpersh);  
} 

foreach ($data20 as $dr ) { 
	$revenue = (array(array("y"=>$dr['Revenue']))); 
	$ProfBfTax = (array(array("y"=>$dr['ProfitBfTax']))); 
	$profatt = (array(array("y"=>$dr['ProfitAttributable']))); 
	$earnps = (array(array("y"=>$dr['EarningsPerShare']))); 
	$ncurrasset = (array(array("y"=>$dr['NonCurrentAsset']))); 
	$currasset = (array(array("y"=>$dr['CurrentAssets']))); 
	$ncurrlia = (array(array("y"=>$dr['NonCurrentLia']))); 
	$currlia = (array(array("y"=>$dr['CurrentLia']))); 
	$attequi = (array(array("y"=>$dr['AttEquity']))); 
	$navpersh = (array(array("y"=>$dr['NavPerShare']))); 
	$dt20 = array_merge($dt20,$revenue); 
	$dt20 = array_merge($dt20,$ProfBfTax); 
	$dt20 = array_merge($dt20,$profatt); 
	$dt20 = array_merge($dt20,$earnps); 
	$dt20 = array_merge($dt20,$ncurrasset); 
	$dt20 = array_merge($dt20,$currasset);  
	$dt20 = array_merge($dt20,$ncurrlia);  
	$dt20 = array_merge($dt20,$currlia);  
	$dt20 = array_merge($dt20,$attequi);  
	$dt20 = array_merge($dt20,$navpersh);  
} 
  
$FY2016 = $Rev; 
$FY2017 = $dt17; 
$FY2018 = $dt18; 
$FY2019 = $dt19; 
$FY2020 = $dt20; 
?>

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/Corporate-Governance-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Informasi Keuangan</h2>
						<p>Investor</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container"> 
			<div class="row justify-content-center">  
				<button class="button active" onClick="overviewFunction(this)"><a>Gambaran umum</a></button>
				<button class="button" onClick="auditFunction(this)"><a>Laporan Tahunan</a></button>
				<button class="button" onClick="remunerationFunction(this)"><a>Laporan keuangan</a></button>
				<button class="button" onClick="nominatingFunction(this)"><a>Sorotan Keuangan</a></button> 
			</div>
		</div>
	</section>
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Overview"> 
						<p>Hasil kuartalan terbaru, informasi dividen dan presentasi untuk Perusahaan DNS dipublikasikan di situs ini. Meskipun dokumen yang terdapat di situs ini telah dipilih dan merupakan kepentingan pemegang saham dan analis riset ekuitas kami saat ini dan di masa mendatang, beberapa hal yang dibahas di situs ini mungkin berisi pernyataan berdasarkan perkiraan yang dibuat pada saat itu. , yang mengandung risiko dan ketidakpastian yang dapat menghasilkan materi yang berbeda dari yang tersurat maupun tersirat dalam pernyataan tersebut.</p>
						<figure class="highcharts-figure">
							<div id="container"></div>
						</figure>    
						
							<p class="highcharts-description">
								Note:
								(1) For comparative purposes, the earnings per share and the NAV per share were computed based on the share capital of 90,000,000 shares upon the listing of the Company.
							</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Audit" style="display: none;">
						<p><b>Salah satu produsen batu bara dengan pertumbuhan tercepat di Indonesia. </b>
						Sejak pembelian konsesi pertambangan pertama oleh pemegang saham pada tahun 1998, DNS telah menunjukkan pertumbuhan yang pesat hingga saat ini, memiliki 5 Perjanjian Kerja Pengusahaan Pertambangan (PKB2B) Batubara dan 16 Izin Usaha Pertambangan (IUP) dengan total wilayah konsesi seluas 126.293 ha. DNS memiliki cadangan dan sumber daya batubara yang besar dan merupakan salah satu pemasok batubara terkemuka di Indonesia. Produk batubara DNS adalah batubara ramah lingkungan yang dikelola secara terintegrasi, mulai dari rencana penambangan hingga pengiriman batubara ke konsumen. Pengendalian kualitas produksi dan fokus pada pelanggan adalah salah satu kekuatan DNS.</p>
						<br>
						<div class="container">
							<div class="row">  
								<div class="col-sm-2">
									<p style="float: right;">Tahun:</p>
								</div>
								<div class="col-sm-10">
									<select onchange="searchFunction()" id="myInput" class="col-sm-4 form-control form-control-sm">
										<option value="" selected>All</option>
										<option value="2021">2021</option>
										<option value="2020">2020</option>
										<option value="2019">2019</option>
										<option value="2018">2018</option>
									</select>
								<br>
								</div>
								<table id="myTable" class="table">
									<tbody><?php foreach($data as $dt) { ?>
										<tr>
											<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="../img/file.png"></td>
											<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
					              		<td style="text-align: center;"><a class="download" href="<?php echo "../admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
											<td hidden><?php echo $dt['Tahun']; ?></td>
										</tr> 
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>  
				</div>
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Remuneration" style="display: none;">
						<p><b>Pertumbuhan produksi batubara Perseroan selama 10 tahun terakhir </b> mengalami pasang surut seiring dengan perkembangan pasar batubara dan waktu yang dibutuhkan untuk membangun infrastruktur yang diperlukan di Tabang. Sejak tahun 2015, Perseroan fokus pada perluasan Tabang, dan pada tahun 2017 Perseroan berhasil meningkatkan produksi secara signifikan menjadi 20,9 MT, atau meningkat 115% dari tahun sebelumnya. Peningkatan ini sejalan dengan strategi kami untuk mengembangkan Tabang menjadi salah satu tambang batu bara terbesar di Indonesia. Perseroan juga memprediksikan peningkatan produksi akan berlanjut hingga 2018, dengan target produksi batu bara antara 24 hingga 28 juta ton.</p>
								<br>
						<div class="container">
							<div class="row">  
								<div class="col-sm-2">
									<p style="float: right;">Tahun:</p>
								</div>
								<div class="col-sm-10">
									<select onchange="searchFunction1()" id="myInput1" class="col-sm-4 form-control form-control-sm">
										<option value="" selected>All</option>
										<option value="2021">2021</option>
										<option value="2020">2020</option>
										<option value="2019">2019</option>
										<option value="2018">2018</option>
									</select>
								<br>
								</div>
								<table id="myTable1" class="table">
									<tbody><?php foreach($dataFS as $dt) { ?>
										<tr>
											<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="../img/file.png"></td>
											<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
					              		<td style="text-align: center;"><a class="download" href="<?php echo "../admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
											<td hidden><?php echo $dt['Tahun']; ?></td>
										</tr> 
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>  
				</div>
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Nominating" style="display: none;">
						<p>Pada tahun 2017, Perseroan mencapai pendapatan sebesar US $ 1.067,4 juta di atas target $ 717,5 juta, melebihi target sebesar 48,8%. Peningkatan pendapatan yang signifikan ini merupakan puncak dari pasar batubara yang kuat, yang menyebabkan kenaikan harga jual batubara (Average Selling Price (ASP) adalah US $ 52,1 per ton, lebih tinggi dari target US $ 40,2 per metrik ton) dan batubara penjualan yang lebih tinggi dari target, dengan penjualan batubara 20,1 juta ton atau di atas target 17,5 juta ton. Ditambah dengan peningkatan pendapatan yang sangat menguntungkan, Perseroan mampu menurunkan biaya kas menjadi US $ 29,0 per metrik ton, lebih rendah dari target sebesar US $ 30,3 per metrik ton. Hasil bersih di atas adalah margin yang sangat positif, yang melebihi target di semua aspek, dan pencapaian margin profitabilitas yang menyaingi dan dalam banyak kasus melebihi perusahaan pertambangan yang lebih besar dan lebih mapan.</p>
								<br>
						<div class="container">
							<div class="row">  
								<div class="col-sm-2">
									<p style="float: right;">Tahun:</p>
								</div>
								<div class="col-sm-10">
									<select onchange="searchFunction2()" id="myInput2" class="col-sm-4 form-control form-control-sm">
										<option value="" selected>All</option>
										<option value="2021">2021</option>
										<option value="2020">2020</option>
										<option value="2019">2019</option>
										<option value="2018">2018</option>
									</select>
								<br>
								</div>
								<table id="myTable2" class="table">
									<tbody><?php foreach($dataFH as $dt) { ?>
										<tr>
											<td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="../img/file.png"></td>
											<td><b><?php echo $dt['Tahun']; ?></b><br /><a><?php echo $dt['Judul']; ?></a></td>
					              		<td style="text-align: center;"><a class="download" href="<?php echo "../admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
											<td hidden><?php echo $dt['Tahun']; ?></td>
										</tr> 
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>  
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>