<?php 
include 'include/header.php'
?>

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="../img/contact-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Hubungi Kami</h2>
						<p> <br /> </p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End --> 

	<!-- Contact Section Begin -->
	<section class="contact-section spad2">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5">
					<div class="contact-text">
						<h4>Jakarta</h4>
						<p style="white-space: pre-line;">Grand ITC Permata Hijau
							Lantai 8 Suite B-7/8
							Kec. Grogol Utara
							Jakarta 12210, Indonesia</p> 
						<div class="ct-item"> 
							<div class="ct-text">
								<h5>Phone</h5>
								<ul>
									<li>+65 6289 6588</li> 
								</ul>
							</div>
						</div>
						<div class="ct-item"> 
							<div class="ct-text">
								<h5>Email</h5>
								<p>info@rgd.sg</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-7 col-md-7">
					<div class="contact-form">
						<h3>Pertanyaan!</h3> 
						<form id="contact-form" method="post" action="./proceed" role="form">
							<input type="text" name="fullname" id="fullname" placeholder="Nama Lengkap">
							<input type="text" name="phonenumber" id="phonenumber" placeholder="Nomor Telpon">
							<input type="text" name="email" id="email" placeholder="Alamat Email">
							<input type="text" name="subject" id="subject" placeholder="Judul">
							<textarea name="message" id="message" placeholder="Pesan"></textarea>

							<div class="form-group">
	                            <div class="g-recaptcha" data-sitekey="6LcInT8aAAAAAEap9qRoTc86qVpTMpMRP_7WiobR" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
	                            <input class="form-control d-none" data-recaptcha="true" required data-error="Please complete the Captcha">
	                            <div class="help-block with-errors"></div>
					      		  <div id="spinner" class="spinner-border text-success" role="status" style="width: 3rem; height: 3rem; display: none;">
							        <span class="sr-only">Loading...</span>
							    </div>
	                        </div>
	                        <div class="messages"></div>
							<button type="submit" class="site-btn">Kirim</button>  
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Contact Section End --> 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>