<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/Quality-Control-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>Quality Control</h2>
						<p>Sustainability</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section set-bg" data-setbg="img/Subbar-Business-Overview.png">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title quotes">
						<h4>We are committed to maintaining high standards of service quality, which is practised across all stages of our business processes for our Coal Trading Business. Stringent processes and quality controls are in place to achieve maximum customer satisfaction.</h4>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"> 
						<div class="container">
							<div class="row">
								<div class="col-xs-12 ">
									<nav>
										<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
											<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Coal trading Business</a>
											<!-- <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Coal Shipping Service</a> -->
										</div>
									</nav>
									<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
										<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
											<section class="testimonial-section">
												<div class="container">
													<div class="row">
														<div class="col-md-6">
															<div class="base"><img class="img-fluid" 
																src="assets/img/Quality Control/coal-trading.jpg" alt="" />
															</div>
														</div>
														<div class="col-md-6 about-text">
															<div class="section-title">
															<p class="info">Each coal delivery shipment is subject to an independent evaluation by a mutually agreed independent surveyor with our customers. The surveyor inspects, evaluates and provides a report on the quality and specifications of coal to be delivered, to certify that such coal meets the requirements of our customers.

																We also have in place in-house customer assessment procedures to evaluate and assess the reputation, credit rating, and payment timeliness of potential customers.</p>
															</div>
														</div>
													</div>
												</div>
											</section> 	
										</div>
										<!-- <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
											<section class="testimonial-section">
												<div class="container">
													<div class="about-text"> 
													<div class="section-title">
														<div class="row">
															<div class="col-md-6">
																<div class="timeline-image"><img class="img-fluid" 
																	src="assets/img/Quality Control/coal-shipping.jpg" alt="" />
																</div>
															</div>
															<div class="col-md-6">
																We adopt a policy of continually refurbishing and repairing our fleet where necessary. This will allow us to maintain operational reliability and minimise expenditure on major fleet repairs and maintenance work over the long term.

																Regular inspection and maintenance are also carried out onboard our vessels by the assigned crew, to ensure that the vessels are in good working condition.
															</div>
														</div>  
													</div>
												 </div> -->
												</div>
											</section>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>